# My project's README

EacObs by Christopher Bull.

### What is this repository for? ###

Analysis of EAC warming events in the observations

### How do I get set up? ###


### Codebase ###

#to populate from vim:
#:r!git ls-tree -r master --name-only


### Who do I talk to? ###

Author: Christopher Bull. 
Affiliation: Climate Change Research Centre and ARC Centre of Excellence for Climate System Science.
             Level 4, Mathews Building
             University of New South Wales
             Sydney, NSW, Australia, 2052
Contact: z3457920@student.unsw.edu.au
www:     christopherbull.com.au
twitter: @@ChrisBullOceanO

