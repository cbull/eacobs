# coding=utf-8
#!/usr/bin/env python 
#   Author: Christopher Bull. 
#   Affiliation: Climate Change Research Centre and ARC Centre of Excellence for Climate System Science.
#                Level 4, Mathews Building
#                University of New South Wales
#                Sydney, NSW, Australia, 2052
#   Contact: z3457920@unsw.edu.au
#   www:     christopherbull.com.au
#   Date created: Tue, 14 Feb 2017 12:57:36
#   Machine created on: chris-VirtualBox2
#

"""
This file is for creating the figure documents to make our EAC IMOS!
"""

from cb2logger import *

import collections
import inputdirs as ind
import mkpaper as mp
import shareme as sm

#just for make_eddy_statistics_table
import itertools

import contextlib as ctx

import os
import stat

import time

if __name__ == "__main__": 
    LogStart('',fout=False)

    ##########
    #  init  #
    ##########

    #check ./inputdirs.py

    #requires mkpaper 42f6ed986e6e8b8b3580a7f5ec10fad5b86d2f21 or later
    nopictures=True
    nopictures=False

    ###########################################################################
    #                        multiple experiments at once!                    #
    ###########################################################################

    if ind.paper_case!='20170211_EACwarmingIMOS':
        lg.error("Not the right paper case!")
        sys.exit()

    output_folder=ind.workdir
    sm.mkdir(output_folder+'paperfigsdoc/')

    #######################
    #  Create the plots!  #
    #######################

    plot_dict=collections.OrderedDict()

    ###########################################################################
    #                              Main Figures                               #
    ###########################################################################
    
    # fn=itertools.cycle(str(r).zfill(2) for r in range(1,99))
    fn=itertools.cycle(str(r) for r in range(1,99))
    # [ path , caption ]

    #/home/z3457920/hdrive/repos/eacobs/diagnostics/mk_obs_comic.py
    fnum='Figure '+fn.next()+'. '
    plot_dict[fnum]=['/srv/ccrc/data49/z3457920/20170211_EACwarmingIMOS/plots/slicelocations.png',fnum+'Fig_intro. ']

    # /home/nfs/z3457920/hdrive/repos/eacobs/diagnostics/find_heatwaves.py
    fnum='Figure '+fn.next()+'. '
    plot_dict[fnum]=['/srv/ccrc/data49/z3457920/20170211_EACwarmingIMOS/plots/hw_EACxOne/MHW_topTen_iCum.png',fnum+'Fig_hws_t10_iCum. ']

    #/home/nfs/z3457920/hdrive/repos/eacobs/diagnostics/pl_imos_hws_attribution.py
    fnum='Figure '+fn.next()+'. '
    plot_dict[fnum]=['/srv/ccrc/data49/z3457920/20170211_EACwarmingIMOS/plots/EACxOne_attr_iCum_mean_10_ssta_vel.png',fnum+'Fig_hw_att. ']
    
    #/home/z3457920/hdrive/repos/eacobs/diagnostics/mk_imos_hovmoller.py
    fnum='Figure '+fn.next()+'. '
    plot_dict[fnum]=['/srv/ccrc/data49/z3457920/20170211_EACwarmingIMOS/plots/hovmoller_imos_v.png',fnum+'Fig_hovmoller_v. ']

    #/home/z3457920/hdrive/repos/eacobs/diagnostics/mk_imos_fldmean.py
    fnum='Figure '+fn.next()+'. '
    plot_dict[fnum]=['/srv/ccrc/data49/z3457920/20170211_EACwarmingIMOS/plots/start_end_timeseries_comparison.png',fnum+'Fig_ssh_climchange. SSH difference between two time-mean periods: 1993/01/01-1995/03/10 (black), : 2015/07/21-2017/01/12 (red). SSH values between 2.1 - 2.3 m are contoured.']

    now = time.strftime("%c")
    figobj=mp.WordFigureDoc(output_folder+'paperfigsdoc/','eacwarm_figures_main',"Working title: Mesoscale drivers of anomolous warming in the EAC extension. \n \n Document compiled on: "+now+"\n")

    ####################
    #  Insert figures  #
    ####################
    
    for fig_num in plot_dict.keys():
        # figobj.add_figure(plot_dict[fig_num][0],plot_dict[fig_num][1],unicodeme=True)
        figobj.add_figure(plot_dict[fig_num][0],plot_dict[fig_num][1],unicodeme=True,nopic=nopictures)

    figobj.end_doc()

    #create copy figure script

    #destdir='/home/nfs/z3457920/Dropbox/Cbull_PapersThesisSupSHARE/20160804_EAC_NoNZ/plots/'
    destdir='/home/nfs/z3457920/Dropbox/Cbull_PapersThesisSupSHARE/20170211_EACwarmingIMOS/plots/'
    with ctx.closing(open(output_folder+'paperfigsdoc/copyplots.sh','w')) as handle:
         handle.write("set -x"+"\n")
         for plot in plot_dict.keys():
             handle.write("cp "+plot_dict[plot][0]+" "+destdir+"\n")

         # handle.write("cp "+output_folder+'paperfigsdoc/'+'nonz_figures_main.docx'+" "+os.path.dirname(os.path.dirname(destdir))+'/'+"\n")

    st = os.stat(output_folder+"paperfigsdoc/copyplots.sh")
    os.chmod(output_folder+"paperfigsdoc/copyplots.sh", st.st_mode | stat.S_IEXEC)
    # subprocess.call("chmod u+x "+output_folder+"paperfigsdoc/copyplots.sh")

    ###########################################################################
    #                     create the supplementary plots!                     #
    ###########################################################################
    
    plot_dict=collections.OrderedDict()
    # fn=itertools.cycle('S'+str(r).zfill(2) for r in range(1,99))
    fn=itertools.cycle('S'+str(r) for r in range(1,99))

    #/home/z3457920/hdrive/repos/eacobs/diagnostics/mk_imos_hovmoller.py
    #relies on /home/z3457920/hdrive/repos/eacobs/diagnostics/mk_imos_eke.py
    fnum='Figure '+fn.next()+'. '
    plot_dict[fnum]=['/srv/ccrc/data49/z3457920/20170211_EACwarmingIMOS/plots/hovmoller_imos_eke.png',fnum+'Fig_hovmoller_eke. ']

    #/home/z3457920/hdrive/repos/eacobs/diagnostics/mk_imos_hovmoller.py
    #relies on /home/z3457920/hdrive/repos/eacobs/diagnostics/mk_imos_eke.py
    fnum='Figure '+fn.next()+'. '
    plot_dict[fnum]=['/srv/ccrc/data49/z3457920/20170211_EACwarmingIMOS/plots/hovmoller_imos_mkeeke.png',fnum+'Fig_hovmoller_mkeeke. ']

    #/home/z3457920/hdrive/repos/eacobs/diagnostics/mk_imos_hovmoller.py
    #relies on /home/z3457920/hdrive/repos/eacobs/diagnostics/mk_imos_eke.py
    fnum='Figure '+fn.next()+'. '
    plot_dict[fnum]=['/srv/ccrc/data49/z3457920/20170211_EACwarmingIMOS/plots/hovmoller_imos_ekemke.png',fnum+'Fig_hovmoller_ekemke. ']

    #/home/z3457920/hdrive/repos/eacobs/diagnostics/mk_imos_hovmoller.py
    fnum='Figure '+fn.next()+'. '
    plot_dict[fnum]=['/srv/ccrc/data49/z3457920/20170211_EACwarmingIMOS/plots/hovmoller_imos_u.png',fnum+'Fig_hovmoller_u. ']

    #/home/z3457920/hdrive/repos/eacobs/diagnostics/mk_imos_hovmoller.py
    fnum='Figure '+fn.next()+'. '
    plot_dict[fnum]=['/srv/ccrc/data49/z3457920/20170211_EACwarmingIMOS/plots/hovmoller_imos_gsla.png',fnum+'Fig_hovmoller_gsla. ']

    #/home/z3457920/hdrive/repos/eacobs/diagnostics/pl_imos_oisst_timeseries.py
    #relies on /home/z3457920/hdrive/repos/eacobs/diagnostics/mk_imos_velocity_timeseries.py and /home/z3457920/hdrive/repos/eacobs/diagnostics/mk_oisst_timeseries.py and /home/z3457920/hdrive/repos/eacobs/diagnostics/mk_imos_eke_timeseries.py
    fnum='Figure '+fn.next()+'. '
    plot_dict[fnum]=['/srv/ccrc/data49/z3457920/20170211_EACwarmingIMOS/plots/imos_vvel_timeseries_raw.png',fnum+'Fig_imos_raw. mean and standard deviation shaded as well as 400 day rolling mean']

    #/home/z3457920/hdrive/repos/eacobs/diagnostics/pl_imos_oisst_timeseries.py
    #relies on /home/z3457920/hdrive/repos/eacobs/diagnostics/mk_imos_velocity_timeseries.py and /home/z3457920/hdrive/repos/eacobs/diagnostics/mk_oisst_timeseries.py and /home/z3457920/hdrive/repos/eacobs/diagnostics/mk_imos_eke_timeseries.py
    fnum='Figure '+fn.next()+'. '
    plot_dict[fnum]=['/srv/ccrc/data49/z3457920/20170211_EACwarmingIMOS/plots/oisst_timeseries_raw.png',fnum+'Fig_oisst_raw. ']

    #/home/z3457920/hdrive/repos/eacobs/diagnostics/pl_imos_oisst_timeseries.py
    #relies on /home/z3457920/hdrive/repos/eacobs/diagnostics/mk_imos_velocity_timeseries.py and /home/z3457920/hdrive/repos/eacobs/diagnostics/mk_oisst_timeseries.py and /home/z3457920/hdrive/repos/eacobs/diagnostics/mk_imos_eke_timeseries.py
    fnum='Figure '+fn.next()+'. '
    plot_dict[fnum]=['/srv/ccrc/data49/z3457920/20170211_EACwarmingIMOS/plots/imoseke_timeseries_raw.png',fnum+'Fig_imoseke_raw. ']

    #/home/z3457920/hdrive/repos/eacobs/diagnostics/pl_imos_oisst_timeseries.py
    #relies on /home/z3457920/hdrive/repos/eacobs/diagnostics/mk_imos_velocity_timeseries.py and /home/z3457920/hdrive/repos/eacobs/diagnostics/mk_oisst_timeseries.py and /home/z3457920/hdrive/repos/eacobs/diagnostics/mk_imos_eke_timeseries.py
    fnum='Figure '+fn.next()+'. '
    plot_dict[fnum]=['/srv/ccrc/data49/z3457920/20170211_EACwarmingIMOS/plots/imos_vvel_summstats.png',fnum+'Fig_imos_sumstats. ']

    #/home/z3457920/hdrive/repos/eacobs/diagnostics/pl_imos_oisst_timeseries.py
    #relies on /home/z3457920/hdrive/repos/eacobs/diagnostics/mk_imos_velocity_timeseries.py and /home/z3457920/hdrive/repos/eacobs/diagnostics/mk_oisst_timeseries.py and /home/z3457920/hdrive/repos/eacobs/diagnostics/mk_imos_eke_timeseries.py
    fnum='Figure '+fn.next()+'. '
    plot_dict[fnum]=['/srv/ccrc/data49/z3457920/20170211_EACwarmingIMOS/plots/oisst_summstats.png',fnum+'Fig_oisst_sumstats. ']

    #/home/z3457920/hdrive/repos/eacobs/diagnostics/pl_imos_oisst_timeseries.py
    #relies on /home/z3457920/hdrive/repos/eacobs/diagnostics/mk_imos_velocity_timeseries.py and /home/z3457920/hdrive/repos/eacobs/diagnostics/mk_oisst_timeseries.py and /home/z3457920/hdrive/repos/eacobs/diagnostics/mk_imos_eke_timeseries.py
    fnum='Figure '+fn.next()+'. '
    plot_dict[fnum]=['/srv/ccrc/data49/z3457920/20170211_EACwarmingIMOS/plots/imoseke_summstats.png',fnum+'Fig_imoseke_sumstats. ']

    #/home/z3457920/hdrive/repos/eacobs/diagnostics/mk_imos_eke.py
    fnum='Figure '+fn.next()+'. '
    plot_dict[fnum]=['/srv/ccrc/data49/z3457920/20170211_EACwarmingIMOS/plots/mke_eke_imos_comp.png',fnum+'Fig_imos_mke_eke_spat. ']

    #/home/z3457920/hdrive/repos/eacobs/diagnostics/mk_imos_eke.py
    fnum='Figure '+fn.next()+'. '
    plot_dict[fnum]=['/srv/ccrc/data49/z3457920/20170211_EACwarmingIMOS/plots/mke_eke_imos_comp_nozoom.png',fnum+'Fig_imos_mke_eke_spat_nozoom. ']

    #/home/z3457920/hdrive/repos/eacobs/diagnostics/mk_imos_eke_monthly.py
    fnum='Figure '+fn.next()+'. '
    plot_dict[fnum]=['/srv/ccrc/data49/z3457920/20170211_EACwarmingIMOS/plots/eke_monthly_imos.png',fnum+'Fig_imos_eke_monthly. Surface monthly EKE, each season is on a separate row.']

    #/home/z3457920/hdrive/repos/eacobs/diagnostics/mk_imos_mke_monthly.py
    fnum='Figure '+fn.next()+'. '
    plot_dict[fnum]=['/srv/ccrc/data49/z3457920/20170211_EACwarmingIMOS/plots/mke_monthly_imos.png',fnum+'Fig_imos_mke_monthly. Surface monthly MKE, each season is on a separate row.']

    #/home/z3457920/hdrive/repos/eacobs/diagnostics/mk_imos_mke_monthly.py
    fnum='Figure '+fn.next()+'. '
    plot_dict[fnum]=['/srv/ccrc/data49/z3457920/20170211_EACwarmingIMOS/plots/mke_monthly_imos_MKEremoved_bias.png',fnum+'Fig_imos_mke_monthly_bias. Surface monthly MKE with time-mean MKE removed, each season is on a separate row.']

    #/home/z3457920/hdrive/repos/eacobs/diagnostics/pl_imos_oisst_timeseries.py
    fnum='Figure '+fn.next()+'. '
    plot_dict[fnum]=['/srv/ccrc/data49/z3457920/20170211_EACwarmingIMOS/plots/imoseke_dayofyear_comp.png',fnum+'Fig_imos_eke_dayofyear.']
    

    now = time.strftime("%c")
    figobj=mp.WordFigureDoc(output_folder+'paperfigsdoc/','eacwarm_figures_supplementary',"\n \n Document compiled on: "+now+"\n")


    for fig_num in plot_dict.keys():
        # figobj.add_figure(plot_dict[fig_num][0],plot_dict[fig_num][1],unicode=True)
        figobj.add_figure(plot_dict[fig_num][0],plot_dict[fig_num][1],unicodeme=True,nopic=nopictures)

    figobj.end_doc()

    #creat copy figure script for supp' plots...
    with ctx.closing(open(output_folder+'paperfigsdoc/copyplots.sh','a')) as handle:
        handle.write(""+"\n")
        handle.write("#supp plots.."+"\n")
        handle.write(""+"\n")
        for plot in plot_dict.keys():
            handle.write("cp "+plot_dict[plot][0]+" "+destdir+"\n")

        handle.write("cp "+output_folder+'paperfigsdoc/'+'eacwarm_figures_main.docx'+" "+os.path.dirname(os.path.dirname(destdir))+'/'+"\n")
        handle.write("cp "+output_folder+'paperfigsdoc/'+'eacwarm_figures_supplementary.docx'+" "+os.path.dirname(os.path.dirname(destdir))+'/'+"\n")
    st = os.stat(output_folder+"paperfigsdoc/copyplots.sh")
    os.chmod(output_folder+"paperfigsdoc/copyplots.sh", st.st_mode | stat.S_IEXEC)


    lg.info('')
    localtime = time.asctime( time.localtime(time.time()) )
    lg.info("Local current time : "+ str(localtime))
    lg.info('SCRIPT ended')
