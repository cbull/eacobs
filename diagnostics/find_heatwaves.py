#!/usr/bin/env python 
#   Author: Christopher Bull. 
#   Affiliation: Climate Change Research Centre and ARC Centre of Excellence for Climate System Science.
#                Level 4, Mathews Building
#                University of New South Wales
#                Sydney, NSW, Australia, 2052
#   Contact: z3457920@unsw.edu.au
#   www:     christopherbull.com.au
#   Date created: Thu, 16 Feb 2017 17:45:25
#   Machine created on: chris-VirtualBox2
#

"""
This file finds the heatwaves using Eric Oliver's marineHeatWaves package
"""

import os,sys
pathfile = os.path.dirname(os.path.realpath(__file__)) 
sys.path.insert(1,os.path.dirname(pathfile)+'/diagnostics/marineHeatWaves/')
import marineHeatWaves as mhw

import collections
from cb2logger import *

import shareme as sm

import inputdirs as ind

import pandas as pd
import numpy as np
from datetime import date
import matplotlib.pyplot as plt

import imgtrkr as it
import os
import datetime
import socket
import inspect

def find_heat(infile,ofilename,pfol):
    """function to have a first go at finding the heatwaves, plots stats about the most intense one
    
    :infile: HDFStore for temperature time series
    :ofilename: region name we are interested in (column from HDFStore)
    :pfol: output folder for the plots
    :returns: None
    """
    lg.info("")
    dataframe=pd.HDFStore(infile)
    df=dataframe.select('df')
    # import ipdb
    # ipdb.set_trace()

    #df=df['1987':'2016']
    ssts=df[ofilename].values.tolist()
    t=[d.to_pydatetime().toordinal() for d in df.index.tolist()]
    t=np.array(t)
    sst=np.array(ssts)
    lg.info("Off to find some heatwaves! For: "+ofilename)
    mhws, clim = mhw.detect(t, sst,climatologyPeriod=[1987,2017])

    lg.info("Stats onsome heatwaves!")
    print 'Number of events:',mhws['n_events']
    print '----'
    ev = np.argmax(mhws['intensity_max']) # Find largest event
    print 'Maximum intensity:', mhws['intensity_max'][ev], 'deg. C'
    print 'Average intensity:', mhws['intensity_mean'][ev], 'deg. C'
    print 'Cumulative intensity:', mhws['intensity_cumulative'][ev], 'deg. C-days'
    print 'Duration:', mhws['duration'][ev], 'days'
    print 'Start date:', mhws['date_start'][ev].strftime("%d %B %Y")
    print 'End date:', mhws['date_end'][ev].strftime("%d %B %Y")

    lg.info("Making some plots of found heatwaves")
    dates = [date.fromordinal(tt.astype(int)) for tt in t]
    plt.figure(figsize=(14,10))
    plt.subplot(2,1,1)
    # Plot SST, seasonal cycle, and threshold
    plt.plot(dates, sst, 'k-')
    plt.plot(dates, clim['thresh'], 'g-')
    plt.plot(dates, clim['seas'], 'b-')
    plt.title('SST (black), seasonal climatology (blue), \
              threshold (green), detected MHW events (shading)')
    plt.xlim(t[0], t[-1])
    plt.ylim(sst.min()-0.5, sst.max()+0.5)
    plt.ylabel(r'SST [$^\circ$C]')
    plt.subplot(2,1,2)
    # Find indices for all ten MHWs before and after event of interest and shade accordingly
    for ev0 in np.arange(ev-10, ev+11, 1):
        t1 = np.where(t==mhws['time_start'][ev0])[0][0]
        t2 = np.where(t==mhws['time_end'][ev0])[0][0]
        plt.fill_between(dates[t1:t2+1], sst[t1:t2+1], clim['thresh'][t1:t2+1], \
                         color=(1,0.6,0.5))
    # Find indices for MHW of interest and shade accordingly
    t1 = np.where(t==mhws['time_start'][ev])[0][0]
    t2 = np.where(t==mhws['time_end'][ev])[0][0]
    plt.fill_between(dates[t1:t2+1], sst[t1:t2+1], clim['thresh'][t1:t2+1], \
                     color='r')
    # Plot SST, seasonal cycle, threshold, shade MHWs with main event in red
    plt.plot(dates, sst, 'k-', linewidth=2)
    plt.plot(dates, clim['thresh'], 'g-', linewidth=2)
    plt.plot(dates, clim['seas'], 'b-', linewidth=2)
    plt.title('SST (black), seasonal climatology (blue), \
              threshold (green), detected MHW events (shading)')
    plt.xlim(mhws['time_start'][ev]-150, mhws['time_end'][ev]+150)
    plt.ylim(clim['seas'].min() - 1, clim['seas'].max() + mhws['intensity_max'][ev] + 0.5)
    plt.ylabel(r'SST [$^\circ$C]')

    path_to_png=pfol+'heatwave_stat1_'+ofilename+'.png'
    plt.savefig(path_to_png,dpi=300,bbox_inches='tight')

    it.AddTrkr(path_to_png,{'Created with':os.path.realpath(__file__),'time':datetime.datetime.now().strftime("%Y-%m-%d %H:%M"),'machine':socket.gethostname(),'function_name':inspect.currentframe().f_code.co_name})

    print path_to_png


    plt.figure(figsize=(15,7))
    # Duration
    plt.subplot(2,2,1)
    evMax = np.argmax(mhws['duration'])
    plt.bar(range(mhws['n_events']), mhws['duration'], width=0.6, \
            color=(0.7,0.7,0.7))
    plt.bar(evMax, mhws['duration'][evMax], width=0.6, color=(1,0.5,0.5))
    plt.bar(ev, mhws['duration'][ev], width=0.6, edgecolor=(1,0.,0.), \
            color='none')
    plt.xlim(0, mhws['n_events'])
    plt.ylabel('[days]')
    plt.title('Duration')
    # Maximum intensity
    plt.subplot(2,2,2)
    evMax = np.argmax(mhws['intensity_max'])
    plt.bar(range(mhws['n_events']), mhws['intensity_max'], width=0.6, \
            color=(0.7,0.7,0.7))
    plt.bar(evMax, mhws['intensity_max'][evMax], width=0.6, color=(1,0.5,0.5))
    plt.bar(ev, mhws['intensity_max'][ev], width=0.6, edgecolor=(1,0.,0.), \
            color='none')
    plt.xlim(0, mhws['n_events'])
    plt.ylabel(r'[$^\circ$C]')
    plt.title('Maximum Intensity')
    # Mean intensity
    plt.subplot(2,2,4)
    evMax = np.argmax(mhws['intensity_mean'])
    plt.bar(range(mhws['n_events']), mhws['intensity_mean'], width=0.6, \
            color=(0.7,0.7,0.7))
    plt.bar(evMax, mhws['intensity_mean'][evMax], width=0.6, color=(1,0.5,0.5))
    plt.bar(ev, mhws['intensity_mean'][ev], width=0.6, edgecolor=(1,0.,0.), \
            color='none')
    plt.xlim(0, mhws['n_events'])
    plt.title('Mean Intensity')
    plt.ylabel(r'[$^\circ$C]')
    plt.xlabel('MHW event number')
    # Cumulative intensity
    plt.subplot(2,2,3)
    evMax = np.argmax(mhws['intensity_cumulative'])
    plt.bar(range(mhws['n_events']), mhws['intensity_cumulative'], width=0.6, \
            color=(0.7,0.7,0.7))
    plt.bar(evMax, mhws['intensity_cumulative'][evMax], width=0.6, color=(1,0.5,0.5))
    plt.bar(ev, mhws['intensity_cumulative'][ev], width=0.6, edgecolor=(1,0.,0.), \
            color='none')
    plt.xlim(0, mhws['n_events'])
    plt.title(r'Cumulative Intensity')
    plt.ylabel(r'[$^\circ$C$\times$days]')
    plt.xlabel('MHW event number')

    path_to_png=pfol+'heatwave_stat2_'+ofilename+'.png'
    plt.savefig(path_to_png,dpi=300,bbox_inches='tight')

    it.AddTrkr(path_to_png,{'Created with':os.path.realpath(__file__),'time':datetime.datetime.now().strftime("%Y-%m-%d %H:%M"),'machine':socket.gethostname(),'function_name':inspect.currentframe().f_code.co_name})
    print path_to_png
    return


def find_detailed_heatstats(infile,ofilename,pfol,coldspells=False):
    """function to find heatwaves, plots stats about the most intense one
    
    :infile: HDFStore for temperature time series
    :ofilename: region name we are interested in (column from HDFStore)
    :pfol: output folder for the plots
    :returns: None

    NB: this has been hacked from Eric's /marineHeatWaves/docs/mhw_stats.py
    """
    col_clim= '0.25'
    col_thresh = 'g-'
    if coldspells:
        mhwname = 'MCS'
        mhwfullname = 'coldspell'
        col_evMax = (0, 102./255, 204./255)
        col_ev = (153./255, 204./255, 1)
        col_bar = (0.5, 0.5, 1)
    else:
        mhwname = 'MHW'
        mhwfullname = 'heatwave'
        col_evMax = 'r'
        col_ev = (1, 0.6, 0.5)
        col_bar = (1, 0.5, 0.5)

    lg.info("")
    dataframe=pd.HDFStore(infile)
    df=dataframe.select('df')

    ssts=df[ofilename].values.tolist()
    t=[d.to_pydatetime().toordinal() for d in df.index.tolist()]
    t=np.array(t)
    dates=[date.fromordinal(tt.astype(int)) for tt in t]
    sst=np.array(ssts)
    lg.info("Off to find some heatwaves! For: "+ofilename)
    mhws, clim = mhw.detect(t, sst,climatologyPeriod=[1987,2017],coldSpells=coldspells)
    mhwBlock = mhw.blockAverage(t, mhws, temp=sst)
    mean, trend, dtrend = mhw.meanTrend(mhwBlock)

    currdir=pfol+'hw_'+ofilename+'/'
    sm.mkdir(currdir)

    # Plot various summary things

    lg.info("Plotting by number..")
    plt.figure(figsize=(15,7))
    plt.subplot(2,2,1)
    evMax = np.argmax(mhws['duration'])
    plt.bar(range(mhws['n_events']), mhws['duration'], width=0.6, color=(0.7,0.7,0.7))
    plt.bar(evMax, mhws['duration'][evMax], width=0.6, color=col_bar)
    plt.xlim(0, mhws['n_events'])
    plt.ylabel('[days]')
    plt.title('Duration')
    plt.subplot(2,2,2)
    evMax = np.argmax(np.abs(mhws['intensity_max']))
    plt.bar(range(mhws['n_events']), mhws['intensity_max'], width=0.6, color=(0.7,0.7,0.7))
    plt.bar(evMax, mhws['intensity_max'][evMax], width=0.6, color=col_bar)
    plt.xlim(0, mhws['n_events'])
    plt.ylabel(r'[$^\circ$C]')
    plt.title('Maximum Intensity')
    plt.subplot(2,2,4)
    evMax = np.argmax(np.abs(mhws['intensity_mean']))
    plt.bar(range(mhws['n_events']), mhws['intensity_mean'], width=0.6, color=(0.7,0.7,0.7))
    plt.bar(evMax, mhws['intensity_mean'][evMax], width=0.6, color=col_bar)
    plt.xlim(0, mhws['n_events'])
    plt.title('Mean Intensity')
    plt.ylabel(r'[$^\circ$C]')
    plt.xlabel(mhwname + ' event number')
    plt.subplot(2,2,3)
    evMax = np.argmax(np.abs(mhws['intensity_cumulative']))
    plt.bar(range(mhws['n_events']), mhws['intensity_cumulative'], width=0.6, color=(0.7,0.7,0.7))
    plt.bar(evMax, mhws['intensity_cumulative'][evMax], width=0.6, color=col_bar)
    plt.xlim(0, mhws['n_events'])
    plt.title(r'Cumulative Intensity')
    plt.ylabel(r'[$^\circ$C$\times$days]')
    plt.xlabel(mhwname + ' event number')
    plt.savefig(currdir + mhwname + '_list_byNumber.png', bbox_inches='tight', pad_inches=0.5, dpi=150)
    print currdir + mhwname + '_list_byNumber.png'

    lg.info("Plotting by date..")
    # ts = date(1982,1,1).toordinal()
    # te = date(2015,3,1).toordinal()
    ts=t[0]
    te=t[-1]
    plt.figure(figsize=(15,7))
    plt.subplot(2,2,1)
    evMax = np.argmax(mhws['duration'])
    plt.bar(mhws['date_peak'], mhws['duration'], width=150, color=(0.7,0.7,0.7))
    plt.bar(mhws['date_peak'][evMax], mhws['duration'][evMax], width=150, color=col_bar)
    plt.xlim(ts, te)
    plt.ylabel('[days]')
    plt.title('Duration')
    plt.subplot(2,2,2)
    evMax = np.argmax(np.abs(mhws['intensity_max']))
    plt.bar(mhws['date_peak'], mhws['intensity_max'], width=150, color=(0.7,0.7,0.7))
    plt.bar(mhws['date_peak'][evMax], mhws['intensity_max'][evMax], width=150, color=col_bar)
    plt.xlim(ts, te)
    plt.ylabel(r'[$^\circ$C]')
    plt.title('Maximum Intensity')
    plt.subplot(2,2,4)
    evMax = np.argmax(np.abs(mhws['intensity_mean']))
    plt.bar(mhws['date_peak'], mhws['intensity_mean'], width=150, color=(0.7,0.7,0.7))
    plt.bar(mhws['date_peak'][evMax], mhws['intensity_mean'][evMax], width=150, color=col_bar)
    plt.xlim(ts, te)
    plt.title('Mean Intensity')
    plt.ylabel(r'[$^\circ$C]')
    plt.subplot(2,2,3)
    evMax = np.argmax(np.abs(mhws['intensity_cumulative']))
    plt.bar(mhws['date_peak'], mhws['intensity_cumulative'], width=150, color=(0.7,0.7,0.7))
    plt.bar(mhws['date_peak'][evMax], mhws['intensity_cumulative'][evMax], width=150, color=col_bar)
    plt.xlim(ts, te)
    plt.title(r'Cumulative Intensity')
    plt.ylabel(r'[$^\circ$C$\times$days]')
    plt.savefig(currdir + mhwname + '_list_byDate.png', bbox_inches='tight', pad_inches=0.5, dpi=150)
    print currdir + mhwname + '_list_byDate.png'

    # Plot top 10 events

    # Maximum intensity
    lg.info("Plotting top ten ..")
    outfile = open(currdir + mhwname + '_topTen_iMax.txt', 'w')
    evs = np.argsort(np.abs(mhws['intensity_max']))[-10:]
    plt.figure(figsize=(23,16))
    for i in range(10):
        ev = evs[-(i+1)]
        plt.subplot(5,2,i+1)
        # Find indices for all ten MHWs before and after event of interest and shade accordingly
        for ev0 in np.arange(max(ev-10,0), min(ev+11,mhws['n_events']-1), 1):
            t1 = np.where(t==mhws['time_start'][ev0])[0][0]
            t2 = np.where(t==mhws['time_end'][ev0])[0][0]
            plt.fill_between(dates[t1:t2+1], sst[t1:t2+1], clim['thresh'][t1:t2+1], color=col_ev)
        # Find indices for MHW of interest (2011 WA event) and shade accordingly
        t1 = np.where(t==mhws['time_start'][ev])[0][0]
        t2 = np.where(t==mhws['time_end'][ev])[0][0]
        plt.fill_between(dates[t1:t2+1], sst[t1:t2+1], clim['thresh'][t1:t2+1], color=col_evMax)
        # Plot SST, seasonal cycle, threshold, shade MHWs with main event in red
        plt.plot(dates, sst, 'k-', linewidth=2)
        plt.plot(dates, clim['thresh'], col_thresh, linewidth=2)
        plt.plot(dates, clim['seas'], col_clim, linewidth=2)
        plt.title('Number ' + str(i+1))
        plt.xlim(mhws['time_start'][ev]-150, mhws['time_end'][ev]+150)
        if coldspells:
            plt.ylim(clim['seas'].min() + mhws['intensity_max'][ev] - 0.5, clim['seas'].max() + 1)
        else:
            plt.ylim(clim['seas'].min() - 1, clim['seas'].max() + mhws['intensity_max'][ev] + 0.5)
        plt.ylabel(r'SST [$^\circ$C]')
        # Save stats
        outfile.write('Number ' + str(i+1) + '\n')
        outfile.write('Maximum intensity: ' + str(mhws['intensity_max'][ev]) + ' deg. C\n')
        outfile.write('Average intensity: '+ str( mhws['intensity_mean'][ev]) + ' deg. C\n')
        outfile.write('Cumulative intensity: ' + str(mhws['intensity_cumulative'][ev]) + ' deg. C-days\n')
        outfile.write('Duration: ' + str(mhws['duration'][ev]) + ' days\n')
        outfile.write('Start date: ' + str(mhws['date_start'][ev].strftime("%d %B %Y")) + '\n')
        outfile.write('End date: ' + str(mhws['date_end'][ev].strftime("%d %B %Y")) + '\n')
        outfile.write('\n')

    plt.legend(['SST', 'threshold', 'seasonal climatology'], loc=4)
    outfile.close()
    plt.savefig(currdir + mhwname + '_topTen_iMax.png', bbox_inches='tight', pad_inches=0.5, dpi=150)
    print currdir + mhwname + '_topTen_iMax.png'

    # Mean intensity
    outfile = open(currdir + mhwname + '_topTen_iMean.txt', 'w')
    evs = np.argsort(np.abs(mhws['intensity_mean']))[-10:]
    plt.clf()
    for i in range(10):
        ev = evs[-(i+1)]
        plt.subplot(5,2,i+1)
        # Find indices for all ten MHWs before and after event of interest and shade accordingly
        for ev0 in np.arange(max(ev-10,0), min(ev+11,mhws['n_events']-1), 1):
            t1 = np.where(t==mhws['time_start'][ev0])[0][0]
            t2 = np.where(t==mhws['time_end'][ev0])[0][0]
            plt.fill_between(dates[t1:t2+1], sst[t1:t2+1], clim['thresh'][t1:t2+1], color=col_ev)
        # Find indices for MHW of interest (2011 WA event) and shade accordingly
        t1 = np.where(t==mhws['time_start'][ev])[0][0]
        t2 = np.where(t==mhws['time_end'][ev])[0][0]
        plt.fill_between(dates[t1:t2+1], sst[t1:t2+1], clim['thresh'][t1:t2+1], color=col_evMax)
        # Plot SST, seasonal cycle, threshold, shade MHWs with main event in red
        plt.plot(dates, sst, 'k-', linewidth=2)
        plt.plot(dates, clim['thresh'], col_thresh, linewidth=2)
        plt.plot(dates, clim['seas'], col_clim, linewidth=2)
        plt.title('Number ' + str(i+1))
        plt.xlim(mhws['time_start'][ev]-150, mhws['time_end'][ev]+150)
        if coldspells:
            plt.ylim(clim['seas'].min() + mhws['intensity_max'][ev] - 0.5, clim['seas'].max() + 1)
        else:
            plt.ylim(clim['seas'].min() - 1, clim['seas'].max() + mhws['intensity_max'][ev] + 0.5)
        plt.ylabel(r'SST [$^\circ$C]')
        # Save stats
        outfile.write('Number ' + str(i+1) + '\n')
        outfile.write('Maximum intensity: ' + str(mhws['intensity_max'][ev]) + ' deg. C\n')
        outfile.write('Average intensity: '+ str( mhws['intensity_mean'][ev]) + ' deg. C\n')
        outfile.write('Cumulative intensity: ' + str(mhws['intensity_cumulative'][ev]) + ' deg. C-days\n')
        outfile.write('Duration: ' + str(mhws['duration'][ev]) + ' days\n')
        outfile.write('Start date: ' + str(mhws['date_start'][ev].strftime("%d %B %Y")) + '\n')
        outfile.write('End date: ' + str(mhws['date_end'][ev].strftime("%d %B %Y")) + '\n')
        outfile.write('\n')

    plt.legend(['SST', 'threshold', 'seasonal climatology'], loc=4)
    outfile.close()
    plt.savefig(currdir + mhwname + '_topTen_iMean.png', bbox_inches='tight', pad_inches=0.5, dpi=150)
    print currdir + mhwname + '_topTen_iMean.png'

    # Cumulative intensity
    outfile = open(currdir + mhwname + '_topTen_iCum.txt', 'w')
    evs = np.argsort(np.abs(mhws['intensity_cumulative']))[-10:]
    plt.clf()
    for i in range(10):
        ev = evs[-(i+1)]
        plt.subplot(5,2,i+1)
        # Find indices for all ten MHWs before and after event of interest and shade accordingly
        for ev0 in np.arange(max(ev-10,0), min(ev+11,mhws['n_events']-1), 1):
            t1 = np.where(t==mhws['time_start'][ev0])[0][0]
            t2 = np.where(t==mhws['time_end'][ev0])[0][0]
            plt.fill_between(dates[t1:t2+1], sst[t1:t2+1], clim['thresh'][t1:t2+1], color=col_ev)
        # Find indices for MHW of interest (2011 WA event) and shade accordingly
        t1 = np.where(t==mhws['time_start'][ev])[0][0]
        t2 = np.where(t==mhws['time_end'][ev])[0][0]
        plt.fill_between(dates[t1:t2+1], sst[t1:t2+1], clim['thresh'][t1:t2+1], color=col_evMax)
        # Plot SST, seasonal cycle, threshold, shade MHWs with main event in red
        plt.plot(dates, sst, 'k-', linewidth=2)
        plt.plot(dates, clim['thresh'], col_thresh, linewidth=2)
        plt.plot(dates, clim['seas'], col_clim, linewidth=2)
        plt.title('Number ' + str(i+1))
        plt.xlim(mhws['time_start'][ev]-150, mhws['time_end'][ev]+150)
        if coldspells:
            plt.ylim(clim['seas'].min() + mhws['intensity_max'][ev] - 0.5, clim['seas'].max() + 1)
        else:
            plt.ylim(clim['seas'].min() - 1, clim['seas'].max() + mhws['intensity_max'][ev] + 0.5)
        plt.ylabel(r'SST [$^\circ$C]')
        # Save stats
        outfile.write('Number ' + str(i+1) + '\n')
        outfile.write('Maximum intensity: ' + str(mhws['intensity_max'][ev]) + ' deg. C\n')
        outfile.write('Average intensity: '+ str( mhws['intensity_mean'][ev]) + ' deg. C\n')
        outfile.write('Cumulative intensity: ' + str(mhws['intensity_cumulative'][ev]) + ' deg. C-days\n')
        outfile.write('Duration: ' + str(mhws['duration'][ev]) + ' days\n')
        outfile.write('Start date: ' + str(mhws['date_start'][ev].strftime("%d %B %Y")) + '\n')
        outfile.write('End date: ' + str(mhws['date_end'][ev].strftime("%d %B %Y")) + '\n')
        outfile.write('\n')

    plt.legend(['SST', 'threshold', 'seasonal climatology'], loc=4)
    outfile.close()
    plt.savefig(currdir + mhwname + '_topTen_iCum.png', bbox_inches='tight', pad_inches=0.5, dpi=150)
    print currdir + mhwname + '_topTen_iCum.png'

    # Duration
    outfile = open(currdir + mhwname + '_topTen_Dur.txt', 'w')
    evs = np.argsort(mhws['duration'])[-10:]
    plt.clf()
    for i in range(10):
        ev = evs[-(i+1)]
        plt.subplot(5,2,i+1)
        # Find indices for all ten MHWs before and after event of interest and shade accordingly
        for ev0 in np.arange(max(ev-10,0), min(ev+11,mhws['n_events']-1), 1):
            t1 = np.where(t==mhws['time_start'][ev0])[0][0]
            t2 = np.where(t==mhws['time_end'][ev0])[0][0]
            plt.fill_between(dates[t1:t2+1], sst[t1:t2+1], clim['thresh'][t1:t2+1], color=col_ev)
        # Find indices for MHW of interest (2011 WA event) and shade accordingly
        t1 = np.where(t==mhws['time_start'][ev])[0][0]
        t2 = np.where(t==mhws['time_end'][ev])[0][0]
        plt.fill_between(dates[t1:t2+1], sst[t1:t2+1], clim['thresh'][t1:t2+1], color=col_evMax)
        # Plot SST, seasonal cycle, threshold, shade MHWs with main event in red
        plt.plot(dates, sst, 'k-', linewidth=2)
        plt.plot(dates, clim['thresh'], col_thresh, linewidth=2)
        plt.plot(dates, clim['seas'], col_clim, linewidth=2)
        plt.title('Number ' + str(i+1))
        plt.xlim(mhws['time_start'][ev]-150, mhws['time_end'][ev]+150)
        if coldspells:
            plt.ylim(clim['seas'].min() + mhws['intensity_max'][ev] - 0.5, clim['seas'].max() + 1)
        else:
            plt.ylim(clim['seas'].min() - 1, clim['seas'].max() + mhws['intensity_max'][ev] + 0.5)
        plt.ylabel(r'SST [$^\circ$C]')
        # Save stats
        outfile.write('Number ' + str(i+1) + '\n')
        outfile.write('Maximum intensity: ' + str(mhws['intensity_max'][ev]) + ' deg. C\n')
        outfile.write('Average intensity: '+ str( mhws['intensity_mean'][ev]) + ' deg. C\n')
        outfile.write('Cumulative intensity: ' + str(mhws['intensity_cumulative'][ev]) + ' deg. C-days\n')
        outfile.write('Duration: ' + str(mhws['duration'][ev]) + ' days\n')
        outfile.write('Start date: ' + str(mhws['date_start'][ev].strftime("%d %B %Y")) + '\n')
        outfile.write('End date: ' + str(mhws['date_end'][ev].strftime("%d %B %Y")) + '\n')
        outfile.write('\n')

    plt.legend(['SST', 'threshold', 'seasonal climatology'], loc=4)
    outfile.close()
    plt.savefig(currdir + mhwname + '_topTen_Dur.png', bbox_inches='tight', pad_inches=0.5, dpi=150)
    print currdir + mhwname + '_topTen_Dur.png'

    # Annual averages
    years = mhwBlock['years_centre']
    plt.figure(figsize=(13,7))
    plt.subplot(2,2,2)
    plt.plot(years, mhwBlock['count'], 'k-')
    plt.plot(years, mhwBlock['count'], 'ko')
    if np.abs(trend['count']) - dtrend['count'] > 0:
         plt.title('Frequency (trend = ' + '{:.2}'.format(10*trend['count']) + '* per decade)')
    else:
         plt.title('Frequency (trend = ' + '{:.2}'.format(10*trend['count']) + ' per decade)')
    plt.ylabel('[count per year]')
    plt.grid()
    plt.subplot(2,2,1)
    plt.plot(years, mhwBlock['duration'], 'k-')
    plt.plot(years, mhwBlock['duration'], 'ko')
    if np.abs(trend['duration']) - dtrend['duration'] > 0:
        plt.title('Duration (trend = ' + '{:.2}'.format(10*trend['duration']) + '* per decade)')
    else:
        plt.title('Duration (trend = ' + '{:.2}'.format(10*trend['duration']) + ' per decade)')
    plt.ylabel('[days]')
    plt.grid()
    plt.subplot(2,2,4)
    plt.plot(years, mhwBlock['intensity_max'], '-', color=col_evMax)
    plt.plot(years, mhwBlock['intensity_mean'], 'k-')
    plt.plot(years, mhwBlock['intensity_max'], 'o', color=col_evMax)
    plt.plot(years, mhwBlock['intensity_mean'], 'ko')
    plt.legend(['Max', 'mean'], loc=2)
    if (np.abs(trend['intensity_max']) - dtrend['intensity_max'] > 0) * (np.abs(trend['intensity_mean']) - dtrend['intensity_mean'] > 0):
        plt.title('Intensity (trend = ' + '{:.2}'.format(10*trend['intensity_max']) + '* (max), ' + '{:.2}'.format(10*trend['intensity_mean'])  + '* (mean) per decade)')
    elif (np.abs(trend['intensity_max']) - dtrend['intensity_max'] > 0):
        plt.title('Intensity (trend = ' + '{:.2}'.format(10*trend['intensity_max']) + '* (max), ' + '{:.2}'.format(10*trend['intensity_mean'])  + ' (mean) per decade)')
    elif (np.abs(trend['intensity_mean']) - dtrend['intensity_mean'] > 0):
        plt.title('Intensity (trend = ' + '{:.2}'.format(10*trend['intensity_max']) + ' (max), ' + '{:.2}'.format(10*trend['intensity_mean'])  + '* (mean) per decade)')
    else:
        plt.title('Intensity (trend = ' + '{:.2}'.format(10*trend['intensity_max']) + ' (max), ' + '{:.2}'.format(10*trend['intensity_mean'])  + ' (mean) per decade)')
    plt.ylabel(r'[$^\circ$C]')
    plt.grid()
    plt.subplot(2,2,3)
    plt.plot(years, mhwBlock['intensity_cumulative'], 'k-')
    plt.plot(years, mhwBlock['intensity_cumulative'], 'ko')
    if np.abs(trend['intensity_cumulative']) - dtrend['intensity_cumulative'] > 0:
        plt.title('Cumulative intensity (trend = ' + '{:.2}'.format(10*trend['intensity_cumulative']) + '* per decade)')
    else:
        plt.title('Cumulative intensity (trend = ' + '{:.2}'.format(10*trend['intensity_cumulative']) + ' per decade)')
    plt.ylabel(r'[$^\circ$C$\times$days]')
    plt.grid()
    plt.savefig(currdir + mhwname + '_annualAverages_meanTrend.png', bbox_inches='tight', pad_inches=0.5, dpi=150)
    print currdir + mhwname + '_annualAverages_meanTrend.png'

    # Save results as text data
    outfile =currdir + mhwname + '_data'

    locations={}
    locations['name']=['cbtest']

    # Event data
    csvfile = open(outfile +'.events.csv', 'w')
    #csvfile.write('# Marine heatwave statistics for individual detected events at [' + str(lon[i]) + ' E ' + str(lat[j]) + ' N] from NOAA OI AVHRR V2 SST data (1982-2014)\n')
    csvfile.write('# Marine ' + mhwfullname + ' statistics for individual detected events at ' + locations['name'][0] + ' from NOAA OI AVHRR V2 SST data (1982-2014)\n')
    csvfile.write('Event number, Start year, Start month, Start day, Peak year, Peak month, Peak day, End year, End month, End day, Duration [days], Maximum intensity [deg C], Mean intensity [deg C], Cumulative intensity [deg C x days], Intensity variability [deg C], Rate of onset [deg C / days], Rate of decline [deg C / days], Maximum intensity (rel. thresh.) [deg C], Mean intensity (rel. thresh.) [deg C], Cumulative intensity (rel. thresh.) [deg C x days], Intensity variability (rel. thresh.) [deg C], Maximum intensity (absolute) [deg C], Mean intensity (absolute) [deg C], Cumulative intensity (absolute) [deg C x days], Intensity variability (absolute) [deg C], Maximum intensity (normalized) [unitless], Mean intensity (normalized) [unitless]\n')
    for ev in range(mhws['n_events']):
        csvfile.write(str(ev+1) + ', ' + str(mhws['date_start'][ev].year) + ', ' + str(mhws['date_start'][ev].month) + ', ' + str(mhws['date_start'][ev].day) + ', ' + str(mhws['date_peak'][ev].year) + ', ' + str(mhws['date_peak'][ev].month) + ', ' + str(mhws['date_peak'][ev].day) + ', ' + str(mhws['date_end'][ev].year) + ', ' + str(mhws['date_end'][ev].month) + ', ' + str(mhws['date_end'][ev].day) + ', ' + str(mhws['duration'][ev]) + ', ' + '{:.4}'.format(mhws['intensity_max'][ev]) + ', ' + '{:.4}'.format(mhws['intensity_mean'][ev]) + ', ' + '{:.4}'.format(mhws['intensity_cumulative'][ev]) + ', ' + '{:.4}'.format(mhws['intensity_var'][ev]) + ', ' + '{:.4}'.format(mhws['rate_onset'][ev]) + ', ' + '{:.4}'.format(mhws['rate_decline'][ev]) + ', ' + '{:.4}'.format(mhws['intensity_max_relThresh'][ev]) + ', ' + '{:.4}'.format(mhws['intensity_mean_relThresh'][ev]) + ', ' + '{:.4}'.format(mhws['intensity_cumulative_relThresh'][ev]) + ', ' + '{:.4}'.format(mhws['intensity_var_relThresh'][ev]) + ', ' + '{:.4}'.format(mhws['intensity_max_abs'][ev]) + ', ' + '{:.4}'.format(mhws['intensity_mean_abs'][ev]) + ', ' + '{:.4}'.format(mhws['intensity_cumulative_abs'][ev]) + ', ' + '{:.4}'.format(mhws['intensity_var_abs'][ev]) + ', ' + '{:.4}'.format(mhws['intensity_max_norm'][ev]) + ', ' + '{:.4}'.format(mhws['intensity_mean_norm'][ev]) + '\n')
    csvfile.close()

    # Annual average data
    csvfile = open(outfile +'.annual.csv', 'w')
    #csvfile.write('# Annual average marine heatwave statistics at [' + str(lon[i]) + ' E ' + str(lat[j]) + ' N] from NOAA OI AVHRR V2 SST data (1982-2014)\n')
    csvfile.write('# Annual average marine ' + mhwfullname + ' statistics at ' + locations['name'][0] + ' from NOAA OI AVHRR V2 SST data (1982-2014)\n')
    csvfile.write('# A value of nan indicates missing data. This should correspond to a year with no MHW events (count = 0)\n')
    return

def export_hw_hdfstore(infile,ofilename,pfol):
    """function to find heatwaves, and export the chosen stats to a HDFStore
    
    :infile: HDFStore for temperature time series
    :ofilename: region name we are interested in (column from HDFStore)
    :pfol: output folder for the HDFStore
    :returns: None

    NB: this has been hacked from Eric's /marineHeatWaves/docs/mhw_stats.py
    """
    currdir=pfol+'hw_'+ofilename+'/'
    sm.mkdir(currdir)

    lg.info("")
    dataframe=pd.HDFStore(infile)
    df=dataframe.select('df')

    ssts=df[ofilename].values.tolist()
    t=[d.to_pydatetime().toordinal() for d in df.index.tolist()]
    t=np.array(t)
    dates=[date.fromordinal(tt.astype(int)) for tt in t]
    sst=np.array(ssts)
    lg.info("Off to find some heatwaves! For: "+ofilename)
    mhws, clim = mhw.detect(t, sst,climatologyPeriod=[1987,2017],coldSpells=False)

    evs = np.argsort(np.abs(mhws['intensity_cumulative']))[-10:]
    iCum=collections.OrderedDict()
    iCum['start']=[]
    iCum['end']=[]
    iCum['ssts']=[]
    for i in range(10):
        ev = evs[-(i+1)]

        t1 = np.where(t==mhws['time_start'][ev])[0][0]
        t2 = np.where(t==mhws['time_end'][ev])[0][0]
        #print i,t1,t2,dates[t1:t2+1][0],dates[t1:t2+1][-1],sst[t1:t2+1][0:13]
        iCum['start'].append(pd.to_datetime(dates[t1:t2+1][0]))
        iCum['end'].append(pd.to_datetime(dates[t1:t2+1][-1]))
        iCum['ssts'].append(sst[t1:t2+1])

    df=pd.DataFrame(iCum)

    efile = currdir+\
            ofilename+'_iCum' +\
            '_table'+ '.h5'
    
    #a clobber check here
    try:
        os.remove(efile)
        lg.info("HDFStore already existed, clobbering!")
    except OSError:
        pass 
    
    #Due to Pandas 'TypeError' had to use put rather than append...
    store = pd.HDFStore(efile,complevel=9, complib='blosc')
    store.put('df',df)
    store.close()
    return


if __name__ == "__main__": 
    LogStart('',fout=False)

    ##########
    #  init  #
    ##########
    
    oisst=\
    ind.oisst

    workdir=\
    ind.workdir

    outfol=workdir+'oisst_temp/'

    plotfol=ind.plotdir

    ##########
    #  main  #
    ##########

    infile=outfol+'tasman_sea_oisst_timeseries_table.h5'
    assert(os.path.exists(infile)),"HDFStore does not exist!"
    df=pd.HDFStore(infile).df
    boxes=df.columns.tolist()
    ##df.close()
    #print boxes
    #for box in boxes:
        #find_heat(infile,box,plotfol)
    #find_heat(infile,'EACxOne',plotfol)
    ## find_heat(infile,'EACxTwo',plotfol)   #not sure why this isn't working :(
    #find_heat(infile,'TFone',plotfol)

    for box in boxes:
        #find_detailed_heatstats(infile,box,plotfol)

        export_hw_hdfstore(infile,box,plotfol)

    lg.info('')
    localtime = time.asctime( time.localtime(time.time()) )
    lg.info("Local current time : "+ str(localtime))
    lg.info('SCRIPT ended')
