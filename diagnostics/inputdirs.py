#!/usr/bin/env python 
#   Author: Christopher Bull. 
#   Affiliation: Climate Change Research Centre and ARC Centre of Excellence for Climate System Science.
#                Level 4, Mathews Building
#                University of New South Wales
#                Sydney, NSW, Australia, 2052
#   Contact: z3457920@unsw.edu.au
#   www:     christopherbull.com.au
#   Date created: Tue, 14 Feb 2017 09:26:35
#   Machine created on: chris-VirtualBox2
#

"""
This module is for adjusting the input dirs and experiments.
"""

import collections
from _indlogger import _LogStart
import collections

_lg=_LogStart().setup()

paper_case='20170211_EACwarmingIMOS'

#this is used by shareme to glob fewer files. If years is undefined, then it will glob everyfile it can. Must only contain two dates
#years=[\
#('1994-01-01','2009-12-31')\
#]

if paper_case=='20170211_EACwarmingIMOS':
    imosfol='/srv/ccrc/data49/z3457920/RawData/IMOS/GSLA/'
    workdir='/srv/ccrc/data49/z3457920/20170211_EACwarmingIMOS/'
    plotdir=workdir+'plots/'
    oisst='/srv/ccrc/data49/z3457920/RawData/OISST/eclipse.ncdc.noaa.gov/'

    #temp=np.squeeze(cdo.sellonlatbox('149,155,-37.8,-35',input=f, returnArray='sst',options='-L'))
    boxdefs=collections.OrderedDict()
    boxdefs['EACxOne']=[149.5,153.5,-37.8,-35.5]
    boxdefs['EACxTwo']=[149.5-2.5,153.5-2.5,-37.8-5.5,-35.5-5.5]
    boxdefs['EACUPOne']=[149.5+3,153+3,-30,-22]
    boxdefs['EACmain']=[150,164,-40,-21]
    boxdefs['SECC']=[164,185,-29,-21]
    boxdefs['NZone']=[173.3+4.5,176+5,-40,-36]

    boxdefs['TFone']=[165,173.3,-32,-37]


    sectiondefs=collections.OrderedDict()
    sectiondefs['A']=(148.2,-42.6) 
    sectiondefs['B']=(150.8,-42.6)
    sectiondefs['C']=(172.5,-40.6)
    sectiondefs['D']=(171.3,-25.8)
    sectiondefs['E']=(155.6,-27.6)
    sectiondefs['F']=(153.6,-27.8)
    sectiondefs['G']=(173.6,-34.9)
    sectiondefs['H']=(146.9,-43.4)
    sectiondefs['I']=(146.9,-45.8)
    sectiondefs['J']=(150.1,-37.1)
    sectiondefs['K']=(151.7,-37.1)
    sectiondefs['L']=(177.1,-37.5)
    sectiondefs['M']=(179.9,-36.8)
else:
    _lg.error("I don't know what to do here!")
    sys.exit()

if __name__ == "__main__":                                     #are we being run directly?
    pass
