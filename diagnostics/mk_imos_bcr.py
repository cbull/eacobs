import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
import os
from netCDF4 import Dataset
meanf='/home/chris/mount_win/hws/IMOS_OceanCurrent_HV_timemean.nc'

ifile=xr.open_dataset(meanf)
ubar=ifile['UCUR'][0,:]
vbar=ifile['VCUR'][0,:]
#ifile.close()

#hilary has a silly message!

f='/home/chris/mount_win/hws/IMOS_OceanCurrent_HV_1993_C-20150521T030649Z.nc'
ifile=xr.open_dataset(f)
u=ifile['UCUR']
v=ifile['VCUR']




print('calc gradient')
grads=np.zeros_like(ifile['UCUR'])
for t in np.arange(np.shape(ifile['UCUR'])[0]):
    #print t
    grads[t,:]=np.gradient(ifile['UCUR'][t,:])[1]+np.gradient(ifile['VCUR'][t,:])[0]

#u' u'
u_prime_sq=np.square(ifile['UCUR']-ubar)

#u' v' * (du/dy + dv/dx)
uv_prime_grads=(ifile['UCUR']-ubar).values*(ifile['VCUR']-vbar).values*grads

#v' v'
v_prime_sq=np.square(ifile['VCUR']-vbar)
btcr=u_prime_sq+uv_prime_grads+v_prime_sq

plt.contourf(btcr[5],30, levels=np.arange(0,0.2,.01),extend='max')
plt.show()
