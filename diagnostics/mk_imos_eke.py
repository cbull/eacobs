# coding=utf-8
#!/usr/bin/env python 
#   Author: Christopher Bull. 
#   Affiliation: Climate Change Research Centre and ARC Centre of Excellence for Climate System Science.
#                Level 4, Mathews Building
#                University of New South Wales
#                Sydney, NSW, Australia, 2052
#   Contact: z3457920@unsw.edu.au
#   www:     christopherbull.com.au
#   Date created: Mon, 13 Feb 2017 17:52:09
#   Machine created on: chris-VirtualBox2
#
"""
This file is designed to create EKE netCDF4 files from IMOS observations.

"""
import logging as lg
import time
import os

import sys

pathfile = os.path.dirname(os.path.realpath(__file__)) 
sys.path.insert(1,os.path.dirname(pathfile)+'/')

from cb2logger import *

import shareme as sm

import inputdirs as ind

from netCDF4 import Dataset
import xarray as xr
import numpy as np
import pandas as pd
import collections
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import gridspec
from matplotlib.ticker import MultipleLocator
from mpl_toolkits.axes_grid1 import make_axes_locatable

import imgtrkr as it
import os
import datetime
import socket
import inspect

import itertools


import collections
import glob
import shareme as sm

import os
import xarray as xr

def pl_mke_eke_comp(timemeanfile,ekef,pfol,zoom=True):
    """@todo: Docstring for pl_mke_eke_comp
    :returns: @todo
    """
    timemean=xr.open_dataset(timemeanfile)
    ubar=timemean['UCUR'][0,:]
    vbar=timemean['VCUR'][0,:]

    mke=0.5*(\
    np.square(ubar)+\
    np.square(vbar)\
            )

    ekefile=xr.open_dataset(ekef)
    eke=ekefile['EKE'][0,:]
    lons=ekefile['LONGITUDE'].values
    lats=ekefile['LATITUDE'].values
    
    plt.close('all')
    row=2
    col=2
    fig, axis = plt.subplots(\
        nrows=row, ncols=col, sharex=False, sharey=False,figsize=(5.5*col,11),\
        gridspec_kw={'hspace':.08,'wspace':.065}\
              )
    
    ax=axis[0,0]
    sm.pl_inset_title_box(ax,'MKE',bwidth="20%",location=3)
    levs=np.linspace(0,0.1,16)
    sm.p_imoslmask(timemeanfile,ax)
    cs1=ax.contourf(lons,lats,mke,levels=levs,extend='max',cmap='jet')
    plt.colorbar(cs1,\
        cax=make_axes_locatable(ax).append_axes("bottom", size="5%", pad=0.25)\
        ,orientation='horizontal')
    if zoom:
        ax.set_xlim([141,183])
        ax.set_ylim([-49,-10])
    sm.change_tick_labels_add_dirs(ax)
    ax=axis[0,1]
    sm.pl_inset_title_box(ax,'EKE',bwidth="20%",location=3)
    sm.p_imoslmask(timemeanfile,ax)
    levs=np.linspace(0,0.1,16)
    cs1=ax.contourf(lons,lats,eke,levels=levs,extend='max',cmap='jet')
    plt.colorbar(cs1,\
        cax=make_axes_locatable(ax).append_axes("bottom", size="5%", pad=0.25)\
        ,orientation='horizontal')
    if zoom:
        ax.set_xlim([141,183])
        ax.set_ylim([-49,-10])
    sm.change_tick_labels_add_dirs(ax)
    plt.setp(ax.get_yticklabels(),visible=False)

    ax=axis[1,0]
    sm.pl_inset_title_box(ax,'MKE/EKE',bwidth="40%",location=3)
    sm.p_imoslmask(timemeanfile,ax)
    levs=np.linspace(0,5,16)
    cs1=ax.contourf(lons,lats,mke/eke,levels=levs,extend='max',cmap='jet')
    plt.colorbar(cs1,\
        cax=make_axes_locatable(ax).append_axes("bottom", size="5%", pad=0.25)\
        ,orientation='horizontal')
    if zoom:
        ax.set_xlim([141,183])
        ax.set_ylim([-49,-10])
    sm.change_tick_labels_add_dirs(ax)
    plt.setp(ax.get_yticklabels(),visible=False)

    ax=axis[1,1]
    sm.pl_inset_title_box(ax,'EKE/MKE',bwidth="40%",location=3)
    sm.p_imoslmask(timemeanfile,ax)
    levs=np.linspace(0,30,16)
    cs1=ax.contourf(lons,lats,eke/mke,levels=levs,extend='max',cmap='jet')
    plt.colorbar(cs1,\
        cax=make_axes_locatable(ax).append_axes("bottom", size="5%", pad=0.25)\
        ,orientation='horizontal')
    if zoom:
        ax.set_xlim([141,183])
        ax.set_ylim([-49,-10])
    sm.change_tick_labels_add_dirs(ax)
    plt.setp(ax.get_yticklabels(),visible=False)
    
    if zoom:
        efile=pfol+'mke_eke_imos_comp.png'
    else:
        efile=pfol+'mke_eke_imos_comp_nozoom.png'
    fig.savefig(efile,dpi=300,bbox_inches='tight')
    it.AddTrkr(efile,{'Created with':os.path.realpath(__file__),'time':datetime.datetime.now().strftime("%Y-%m-%d %H:%M"),'machine':socket.gethostname(),'function_name':inspect.currentframe().f_code.co_name})
    print efile

    return


if __name__ == "__main__":                                     #are we being run directly?
    LogStart('',fout=False)
    ###########################################################################
    #                                  init                                   #
    ###########################################################################

    imosfol=\
    ind.imosfol

    workdir=\
    ind.workdir

    outdir=workdir+'imos_eke/'

    plotfol=ind.plotdir
    sm.mkdir(plotfol)
    sm.mkdir(outdir)

    timemeanf=sm.get_imos_time_mean()

    ###########################################################################
    #                                  main                                   #
    ###########################################################################

    if ind.paper_case=='20170211_EACwarmingIMOS':
        dfidx=sm.construct_imos_index()

        timemean=xr.open_dataset(timemeanf)
        ubar=timemean['UCUR'][0,:]
        vbar=timemean['VCUR'][0,:]

        mke=0.5*(\
        np.square(ubar)+\
        np.square(vbar)\
                )

        for f in dfidx['file'].drop_duplicates().values.tolist():
            assert(os.path.exists(f)),"netCDF file does not exist!"
            ifile=xr.open_dataset(f)

            lg.info("Calculating EKE :" + os.path.basename(f))

            eke=0.5*(\
            np.square(ifile['UCUR']-ubar)+\
            np.square(ifile['VCUR']-vbar)\
                    )
            #eke=np.swapaxes(np.swapaxes(eke,0,2),1,2)
            #eke=np.swapaxes(eke,0,2)

            eke_mke=np.zeros_like(eke)
            for t in np.arange(np.shape(eke_mke)[0]):
                eke_mke[t,:,:]=eke.values[t,:,:]/(eke[t,:,:]+mke)
            #plt.contourf(eke_mke[15,:,:],30,levels=np.linspace(0,10,5))
            #plt.show()

            mke_eke=np.zeros_like(eke)
            for t in np.arange(np.shape(mke_eke)[0]):
                mke_eke[t,:,:]=mke/(mke+eke.values[t,:,:])

            lg.info("Calculating BCR :" + os.path.basename(f))

            grads=np.zeros_like(ifile['UCUR'])
            for t in np.arange(np.shape(ifile['UCUR'])[0]):
                #print t
                grads[t,:]=np.gradient(ifile['UCUR'][t,:])[1]+np.gradient(ifile['VCUR'][t,:])[0]

            #u' u'
            u_prime_sq=np.square(ifile['UCUR']-ubar)

            #u' v' * (du/dy + dv/dx)
            uv_prime_grads=(ifile['UCUR']-ubar).values*(ifile['VCUR']-vbar).values*grads

            #v' v'
            v_prime_sq=np.square(ifile['VCUR']-vbar)
            btcr=u_prime_sq+uv_prime_grads+v_prime_sq

            lg.info("Outputing EKE :" +outdir+os.path.basename(f))
            del ifile['GSLA']
            del ifile['GSL']
            del ifile['UCUR']
            #if trying to pick up an existing netcdf file and change something..
            ifile['EKE']=xr.DataArray(eke,dims=('TIME','LATITUDE','LONGITUDE'),coords=ifile.VCUR.coords)

            ifile['BTCR']=xr.DataArray(btcr,dims=('TIME','LATITUDE','LONGITUDE'),coords=ifile.VCUR.coords)

            ifile['EKEMKE']=xr.DataArray(eke_mke,dims=('TIME','LATITUDE','LONGITUDE'),coords=ifile.VCUR.coords)
            ifile['MKEEKE']=xr.DataArray(mke_eke,dims=('TIME','LATITUDE','LONGITUDE'),coords=ifile.VCUR.coords)
            del ifile['VCUR']
            ifile.to_netcdf(outdir+os.path.basename(f))
            ifile.close()

        lg.info("Creating time-mean EKE file: " + ind.workdir+'IMOS_OceanCurrent_HV_eke_timemean.nc')
        meanekef=sm.get_imos_eke_time_mean()

        pl_mke_eke_comp(timemeanf,meanekef,plotfol)
        pl_mke_eke_comp(timemeanf,meanekef,plotfol,zoom=False)

    else:
        lg.error("I don't know what to do here!")
        sys.exit()


    lg.info('')
    localtime = time.asctime( time.localtime(time.time()) )
    lg.info("Local current time : "+ str(localtime))
    lg.info('SCRIPT ended')
