# coding=utf-8
#!/usr/bin/env python 
#   Author: Christopher Bull. 
#   Affiliation: Climate Change Research Centre and ARC Centre of Excellence for Climate System Science.
#                Level 4, Mathews Building
#                University of New South Wales
#                Sydney, NSW, Australia, 2052
#   Contact: z3457920@unsw.edu.au
#   www:     christopherbull.com.au
#   Date created: Mon, 13 Feb 2017 17:52:09
#   Machine created on: chris-VirtualBox2
#
"""
This file is designed to extract a time series of eke from pre-defined regions

You need to run ./mk_imos_eke.py

"""
import logging as lg
import time
import os

import sys

pathfile = os.path.dirname(os.path.realpath(__file__)) 
sys.path.insert(1,os.path.dirname(pathfile)+'/')

from cb2logger import *

import shareme as sm

import inputdirs as ind

from netCDF4 import Dataset
import numpy as np
import pandas as pd
import collections
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import gridspec
from matplotlib.ticker import MultipleLocator

from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.ticker import MultipleLocator

#import imgtrkr as it
import os
import datetime
import socket
import inspect

import itertools

from mpl_toolkits.axes_grid.inset_locator import inset_axes

import collections
#import vorticity
import glob
import shareme as sm

import xarray as xr

if __name__ == "__main__":
    LogStart('',fout=False)
    ##########
    #  init  #
    ##########
    

    workdir=\
    ind.workdir

    imoseke=\
    ind.workdir+'imos_eke/'

    outfol=workdir+'imos_eke/'

    plotfol=ind.plotdir
    sm.mkdir(plotfol)
    sm.mkdir(outfol)

    #################
    #  The Work...  #
    #################

    # Load required module
    from cdo import *
    cdo = Cdo()
    cdo.debug = False

    ifiles=sorted(glob.glob(imoseke+ '*.nc'))
    assert(ifiles!=[]),"glob didn't find anything!"

    regions=collections.OrderedDict()
    for boxname in ind.boxdefs.keys():
        exme=''.join(str(ind.boxdefs[boxname]))
        exme=exme[1:-1]
        exme=exme.replace(' ','')
        exme=exme+' '

        means=collections.OrderedDict()
        for f in ifiles:
            lg.info("Boxname: "+boxname +' from file: ' + os.path.basename(f))
            #cdo sellonlatbox,149,155,-37.8,-35 avhrr-only-v2.19940430.nc
            eke=np.squeeze(cdo.sellonlatbox(exme,input=f, returnArray='EKE',options='-L'))

            ifile=xr.open_dataset(f)
            for idx,time in enumerate(ifile['TIME'][:].values):
                means[pd.to_datetime(time)]=np.nanmean(eke[idx,:])
            ifile.close()
        regions[boxname]=means

    df=pd.DataFrame(regions)
    efile = outfol +\
            'tasman_sea_imoseke_timeseries'+\
            '_table'+ '.h5'
    
    #a clobber check here
    try:
        os.remove(efile)
        lg.info("HDFStore already existed, clobbering!")
    except OSError:
        pass 
    
    print efile
    #Due to Pandas 'TypeError' had to use put rather than append...
    store = pd.HDFStore(efile,complevel=9, complib='blosc')
    store.put('df',df)
    store.close()


    lg.info('')
    #localtime = time.asctime( time.localtime(time.time()) )
    #lg.info("Local current time : "+ str(localtime))
    lg.info('SCRIPT ended')
