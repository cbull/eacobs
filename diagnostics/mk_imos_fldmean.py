# coding=utf-8
#!/usr/bin/env python 
#   Author: Christopher Bull. 
#   Affiliation: Climate Change Research Centre and ARC Centre of Excellence for Climate System Science.
#                Level 4, Mathews Building
#                University of New South Wales
#                Sydney, NSW, Australia, 2052
#   Contact: z3457920@unsw.edu.au
#   www:     christopherbull.com.au
#   Date created: Mon, 13 Feb 2017 17:52:09
#   Machine created on: chris-VirtualBox2
#
"""
This file is designed to extract a time series of oisst from pre-defined regions

"""
import logging as lg
import time
import os

import sys

pathfile = os.path.dirname(os.path.realpath(__file__)) 
sys.path.insert(1,os.path.dirname(pathfile)+'/')

from cb2logger import *

import shareme as sm

import inputdirs as ind

import numpy as np
import pandas as pd
import collections
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import gridspec
from matplotlib.ticker import MultipleLocator

from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.ticker import MultipleLocator

#import imgtrkr as it
import os
import datetime
import socket
import inspect

import itertools

from mpl_toolkits.axes_grid.inset_locator import inset_axes

import collections
#import vorticity
import glob
import shareme as sm

#import xarray as xr
from netCDF4 import Dataset

import imgtrkr as it
import os
import datetime
import socket
import inspect

def plot_mean_fldcontour_comparison(ax0,e1,e2,n1,n2,dir_dict,lat_lon_file,inset=False):
    """@todo: Docstring for plot_mean_fldcontour_comparison
    
    :exps: @todo
    :returns: @todo
    """

    mean_arrays={}
    for exp in [e1,e2]:
        lg.info("working experiment: " + exp)
        onamemean=output_folder+'nemo_mean_std_nparray/'+exp+'_nemo_mean'+'_'+ind.years[0][0]+'_'+ind.years[0][1]+'.npy'
        assert(os.path.exists(onamemean)),"can't find mean file"

        mean_arrays[exp]=np.load(onamemean)

        #remove backgorund mean..
        mean_arrays[exp]=mean_arrays[exp]-np.mean(mean_arrays[exp])


    lmask = np.ma.masked_where(
        mean_arrays['nemo_cordex24_ERAI01'] ==0.,
        mean_arrays['nemo_cordex24_ERAI01']) 


    assert(os.path.exists(lat_lon_file)),"netCDF file does not exist!"
    ifile=Dataset(lat_lon_file, 'r')
    lat=ifile.variables['nav_lat'][:]
    lon=sm.nemo_fixdateline(ifile)
    ifile.close()

    #set up for gridspec plot...
    # plt.close('all')
    #width then height
    # fig=plt.figure(figsize=(14.0,10.0))
    
    # gs = gridspec.GridSpec(1, 1,hspace=.12,wspace=0.04)

    matplotlib.rcParams['contour.negative_linestyle'] = 'solid'
    # ax0 = plt.subplot(gs[0,0])

    ecolors,ecolrs_names=sm.exp_colours_nonz(hex=True)
    globalpha=0.8
    cntlinesalpha=0.6
    cntlinesrange=(-0.1,0.1)

    mean_arrays[e1]=np.ma.masked_where(lmask.mask,mean_arrays[e1]).filled()

    cs1=ax0.contour(lon,lat,mean_arrays[e1],colors=ecolors[e1],levels=np.linspace(cntlinesrange[0],cntlinesrange[1],2),alpha=0.9,linewidths=4)
    cs1=ax0.contour(lon,lat,mean_arrays[e1],colors=ecolors[e1],levels=np.linspace(-0.1,0.1,11),alpha=cntlinesalpha,linewidths=[3]+[1]*9+[3])
    cs1=ax0.contourf(lon,lat,mean_arrays[e1],colors=ecolors[e1],levels=np.linspace(-0.1,0.1,2),alpha=cntlinesalpha)

    cs1=ax0.contour(lon,lat,lmask.filled(),colors=ecolors[e2],levels=np.linspace(cntlinesrange[0],cntlinesrange[1],2),alpha=0.8,linewidths=3)

    tfile=sorted(glob.glob(dir_dict[e1][0]+'1989/*grid_T_2D.nc' ))
    assert(tfile!=[]),"glob didn't find anything!"

    bathyfiles=sorted(glob.glob(dir_dict[e1][0]+'bathy_meter*.nc' ))
    assert(bathyfiles!=[]),"glob didn't find anything!"
    ifile=Dataset(bathyfiles[0], 'r')
    bathy=ifile.variables['Bathymetry'][:]
    bathy_cmap=ax0.contour(lon,lat,bathy,30,alpha=0.2,levels=np.arange(0,1000,200),colors='k')
    bathy_cmap=ax0.contour(lon,lat,bathy,30,alpha=0.2,levels=np.arange(1000,9000,1000),colors='k')
    # #gray #D9D9D9

    if not inset:
        ax0.set_xlim([140,190])
        ax0.set_ylim([-50.5,-23])
    sections=ind.section_description_dict
    sections={k: sections[k] for k in ('AB', 'BC', 'GD','FE','ED')}
    # sm.pl_depth_sect_lines_from_inputdirs(ax0,zoom=[140,190,-47,-20],sections=sections,nolandmask=True)

    if not inset:
        sm.ins_legend_nonz(ax0,exp_subset=[n2,n1],lw=3,location=1,invisible=False,ncols=2)
        sm.change_tick_labels_add_dirs(ax0)

    # plt.show()
    # # import ipdb
    # # ipdb.set_trace()

    # oname=plot_output_folder+'zos_mean_fldmean_'+n1+'bathy.png'
    # plt.savefig(oname,dpi=300)
    # it.AddTrkr(oname,{'Created with':os.path.realpath(__file__),'time':datetime.datetime.now().strftime("%Y-%m-%d %H:%M"),'machine':socket.gethostname()})
    # print oname

    ifile.close()
    return

def plot_fld_cntr_diff(old,new,maskfile,pfol):
    """@todo: Docstring for plot_fld_cntr_diff
    
    :old: @todo
    :new: @todo
    :returns: @todo
    """
    plt.close('all')
    fig=plt.figure()
    ax=fig.add_subplot(1, 1,1)

    ifile=Dataset(maskfile, 'r')
    lons=ifile.variables['LONGITUDE'][:]
    lats=ifile.variables['LATITUDE'][:]
    x,y=np.meshgrid(lons,lats)
    cntlinesalpha=0.6

    old= np.ma.masked_where(
        ifile.variables['UCUR'][0,:].mask ==True,
        old ) 

    new= np.ma.masked_where(
        ifile.variables['UCUR'][0,:].mask ==True,
        new) 

    cs1=ax.contour(lons,lats,old,colors='k',levels=np.linspace(2.1,2.3,11),alpha=cntlinesalpha,linewidths=[3]+[1]*9+[3])
    cs1=ax.contourf(lons,lats,old,colors='k',levels=np.linspace(2.1,2.3,2),alpha=cntlinesalpha)
    cs1=ax.contour(lons,lats,new,colors='r',levels=np.linspace(2.1,2.3,11),alpha=cntlinesalpha,linewidths=[3]+[1]*9+[3])
    cs1=ax.contourf(lons,lats,new,colors='r',levels=np.linspace(2.1,2.3,2),alpha=cntlinesalpha)

    #sm.pl_inset_title_box(ax,'time-mean EAC surface velocity',bwidth="60%",location=4)
    sm.p_imoslmask(maskfile,ax)
    sm.change_tick_labels_add_dirs(ax)
    ax.set_xlim([141,183])
    ax.set_ylim([-49,-27])

    path_to_png=pfol+'start_end_timeseries_comparison.png'
    fig.savefig(path_to_png,dpi=300,bbox_inches='tight')
    #fig.savefig('./.pdf',format='pdf')
    it.AddTrkr(path_to_png,{'Created with':os.path.realpath(__file__),'time':datetime.datetime.now().strftime("%Y-%m-%d %H:%M"),'machine':socket.gethostname(),'function_name':inspect.currentframe().f_code.co_name})
    print path_to_png
    return


if __name__ == "__main__":
    LogStart('',fout=False)
    ##########
    #  init  #
    ##########
    
    oisst=\
    ind.oisst

    workdir=\
    ind.workdir

    plotfol=ind.plotdir
    sm.mkdir(plotfol)

    #################
    #  The Work...  #
    #################

    dfidx=sm.construct_imos_index()

    if not os.path.exists(workdir+'imos_fldmean_fldstart.npy'):
        lg.info("Creating support files: "+workdir+'imos_fldmean_fldstart.npy and '+workdir+'imos_fldmean_fldend.npy')
        mean_array=[]
        for date,row in dfidx.iloc[0:400].iterrows():
            #print date
            #ifile=xr.open_dataset(row['file'])
            #mean_array.append(ifile['GSL'][row['tidx'],:])
            ifile=Dataset(row['file'], 'r')
            mean_array.append(ifile.variables['GSL'][row['tidx'],:])
        fldstart=np.mean(mean_array,axis=0)

        mean_array=[]
        for date,row in dfidx.iloc[-400:].iterrows():
            ifile=Dataset(row['file'], 'r')
            mean_array.append(ifile.variables['GSL'][row['tidx'],:])
        fldend=np.mean(mean_array,axis=0)
        np.save(workdir+'imos_fldmean_fldstart',fldstart)
        np.save(workdir+'imos_fldmean_fldend',fldend)
    else:
        lg.info("Support files, alerady existed, so loading...")

    fldstart=np.load(workdir+'imos_fldmean_fldstart.npy')
    fldend=np.load(workdir+'imos_fldmean_fldend.npy')

    print 'time-mean first range: ',dfidx.iloc[0:400].iloc[0]['date'],dfidx.iloc[0:400].iloc[-1]['date']
    print 'time-mean last range: ',dfidx.iloc[-400:].iloc[0]['date'],dfidx.iloc[-400:].iloc[-1]['date']
    plot_fld_cntr_diff(fldstart,fldend,dfidx.iloc[0]['file'],plotfol)

    lg.info('')
    localtime = time.asctime( time.localtime(time.time()) )
    lg.info("Local current time : "+ str(localtime))
    lg.info('SCRIPT ended')
