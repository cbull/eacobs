# coding=utf-8
#!/usr/bin/env python 
#   Author: Christopher Bull. 
#   Affiliation: Climate Change Research Centre and ARC Centre of Excellence for Climate System Science.
#                Level 4, Mathews Building
#                University of New South Wales
#                Sydney, NSW, Australia, 2052
#   Contact: z3457920@unsw.edu.au
#   www:     christopherbull.com.au
#   Date created: Mon, 13 Feb 2017 17:52:09
#   Machine created on: chris-VirtualBox2
#
"""
This file is designed to plot hovmollers of various IMOS observations.

If you also want to plot EKE metrics you need to have run
    /home/z3457920/hdrive/repos/eacobs/diagnostics/mk_imos_eke.py
    Thus, there should be files in: workdir+'imos_eke'
"""
import logging as lg
import time
import os

import sys

pathfile = os.path.dirname(os.path.realpath(__file__)) 
sys.path.insert(1,os.path.dirname(pathfile)+'/')

from cb2logger import *

import shareme as sm

import inputdirs as ind


from netCDF4 import Dataset
import xarray as xr
import numpy as np
import pandas as pd
import collections
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import gridspec
from matplotlib.ticker import MultipleLocator

from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.ticker import MultipleLocator

#import imgtrkr as it
import os
import datetime
import socket
import inspect

import itertools

from mpl_toolkits.axes_grid.inset_locator import inset_axes

import collections
#import vorticity
import glob
import shareme as sm


def extract_times(timemeanfile,eke=False):
    """function to extract time series and create hdfstore
    :eke=False: also extract time series of eke metrics
    :returns: @todo
    """
    def exportme(fieldname,pathadd):
        dfdict=collections.OrderedDict()
        dfdict['lon']=lons
        dfdict['lat']=lats

        for tcount in np.arange(len(times)):
            dfdict[times[tcount]]=fieldname[tcount]

        df=pd.DataFrame(dfdict)
        efile = outfol +\
                'imos_output_'+pathadd +\
                '_table'+ '.h5'
        
        #a clobber check here
        try:
            os.remove(efile)
            lg.info("HDFStore already existed, clobbering!")
        except OSError:
            pass 
        
        #Due to Pandas 'TypeError' had to use put rather than append...
        store = pd.HDFStore(efile,complevel=9, complib='blosc')
        store.put('df',df)
        store.close()
        lg.info("HDFStore created: " + efile)
        return

    ifiles=sorted(glob.glob(imosfol + 'IM*.nc' ))
    assert(ifiles!=[]),"glob didn't find anything!"

    if eke:
        ekefiles=sorted(glob.glob(workdir+ 'imos_eke/IM*.nc' ))
        assert(ekefiles!=[]),"glob didn't find anything! Have you run mk_imos_eke.py?"

    #removing the time mean from the hovmoller list...
    #if timemeanfile in ifiles:
        #ifiles.remove(timemeanfile)

    assert(os.path.exists(timemeanfile)),"Can't find your time mean file, have you made it?"
    ifile=Dataset(timemeanfile, 'r')

    startlat=sm.find_nearest(ifile.variables['LATITUDE'][:],-27)
    endlat=sm.find_nearest(ifile.variables['LATITUDE'][:],-37.5)
    startlong=sm.find_nearest(ifile.variables['LONGITUDE'][:],140)

    latsidx=[]
    lonsidx=[]

    lats=[]
    lons=[]
    for lat in np.arange(startlat,endlat,-1):
        for kick in np.arange(100):
            #print kick
            try:
                ifile.variables['VCUR'][0,lat,startlong+kick].mask
            except AttributeError, e:
                #raise e

                #searches for max time-mean southward velocity within 2.5 degrees of the coast..
                fcore=np.argmin(ifile.variables['VCUR'][0,lat,startlong+kick:startlong+kick+12])

                lats.append(ifile.variables['LATITUDE'][lat])
                lons.append(ifile.variables['LONGITUDE'][startlong+kick+fcore])

                latsidx.append(lat)
                lonsidx.append(startlong+kick+fcore)
                break

    #check plot
    #plt.close('all')
    #fig=plt.figure()
    #ax=fig.add_subplot(1, 1,1)
    #ax.contourf(ifile.variables['LONGITUDE'][:],ifile.variables['LATITUDE'][:],ifile.variables['GSLA'][0,:])
    #for idx in np.arange(len(lats)):
        #ax.scatter(lons[idx],lats[idx])
    #plt.show()
    ifile.close()

    if not eke:
        field=[]
        fieldtwo=[]
        fieldthree=[]
        times=[]
        for f in ifiles:
            #print f
            lg.info('Working file: ' + f)
            ifile=xr.open_dataset(f)

            v=\
            [ifile.variables['VCUR'][:,j,i] for j,i in zip(latsidx,lonsidx)]
            v=np.swapaxes(v,0,1)

            gsla=\
            [ifile.variables['GSLA'][:,j,i] for j,i in zip(latsidx,lonsidx)]
            gsla=np.swapaxes(gsla,0,1)

            u=\
            [ifile.variables['UCUR'][:,j,i] for j,i in zip(latsidx,lonsidx)]
            u=np.swapaxes(u,0,1)

            times.append(ifile['TIME'][:].values)
            field.append(v)
            fieldtwo.append(gsla)
            fieldthree.append(u)
            ifile.close()

        times=np.hstack(times)
        field=np.vstack(field)
        fieldtwo=np.vstack(fieldtwo)
        fieldthree=np.vstack(fieldthree)

        exportme(field,'u')
        exportme(fieldtwo,'gsla')
        exportme(fieldthree,'v')
    else:
        fieldfour=[]
        fieldfive=[]
        fieldsix=[]
        times=[]
        for f in ekefiles:
            #print f
            lg.info('Working file (EKE): ' + f)
            ifile=xr.open_dataset(f)

            eke=\
            [ifile.variables['EKE'][:,j,i] for j,i in zip(latsidx,lonsidx)]
            eke=np.swapaxes(eke,0,1)

            ekemke=\
            [ifile.variables['EKEMKE'][:,j,i] for j,i in zip(latsidx,lonsidx)]
            ekemke=np.swapaxes(ekemke,0,1)

            mkeeke=\
            [ifile.variables['MKEEKE'][:,j,i] for j,i in zip(latsidx,lonsidx)]
            mkeeke=np.swapaxes(mkeeke,0,1)

            times.append(ifile['TIME'][:].values)
            fieldfour.append(eke)
            fieldfive.append(ekemke)
            fieldsix.append(mkeeke)
            ifile.close()

        #NB: timestwo doesn't get used, should really make an error trap
        #lg.warning("EKE files are assumed to have the same time!")

        times=np.hstack(times)
        fieldfour=np.vstack(fieldfour)
        fieldfive=np.vstack(fieldfive)
        fieldsix=np.vstack(fieldsix)

        exportme(fieldfour,'eke')
        exportme(fieldfive,'ekemke')
        exportme(fieldsix,'mkeeke')

    return

def plothovs_imos(outfol,pathadd,plotfolder):
    """@todo: Docstring for plothovs_imos
    
    :outfol: @todo
    :pathadd: @todo
    :returns: @todo
    """
    efile = outfol +\
            'imos_output_'+pathadd +\
            '_table'+ '.h5'
    dataframe=pd.HDFStore(efile)
    df=dataframe.select('df')

    plt.close('all')
    fig=plt.figure()
    ax=fig.add_subplot(1, 1,1)
    x=df.lat
    y=[pd.to_datetime(t) for t in df.columns[2:].tolist()]
    vmin=-0.5
    vmax=0.5
    levies=np.arange(vmin,vmax+0.05,0.05).tolist()
    ccmap='seismic'
    if pathadd=='eke':
        levies=np.linspace(0,0.1,16)
        ccmap='jet'
    elif pathadd=='ekemke':
        levies=np.linspace(0,4,16)
        ccmap='jet'
    elif pathadd=='mkeeke':
        levies=np.linspace(0,5,16)
        ccmap='jet'
    
    z=[df[a] for a in df.columns[2:].tolist()]
    cs1=ax.contourf(y,x,np.transpose(z),levels=levies,cmap=ccmap,extend='both',alpha=0.9)
    ax.set_title(pathadd)

    plt.colorbar(cs1,cax=make_axes_locatable(ax).append_axes("bottom", size="5%", pad=0.25),orientation='horizontal')

    ofile=plotfolder+'hovmoller_imos_'+pathadd +'.png'
    fig.savefig(ofile,dpi=300,bbox_inches='tight')
    print ofile
    return

if __name__ == "__main__":                                     #are we being run directly?
    LogStart('',fout=False)
    ###########################################################################
    #                                  init                                   #
    ###########################################################################

    imosfol=\
    ind.imosfol
    #'/srv/ccrc/data49/z3457920/RawData/IMOS/GSLA/'

    workdir=\
    ind.workdir
    #'/srv/ccrc/data49/z3457920/20170211_EACwarmingIMOS/'

    outfol=workdir+'hovmollers/'

    plotfol=ind.plotdir
    sm.mkdir(plotfol)
    sm.mkdir(outfol)

    timemeanf=sm.get_imos_time_mean()

    ###########################################################################
    #                                  main                                   #
    ###########################################################################

    if ind.paper_case=='20170211_EACwarmingIMOS':
        #dfidx=sm.construct_imos_index()

        #this assumes if one exists they all exist!
        if not os.path.exists(outfol+'imos_output'+'_v_table'+ '.h5'):
            lg.info("Creating support files (for Hovmollers)")
            extract_times(timemeanf,eke=False)
            extract_times(timemeanf,eke=True)
        else:
            lg.warning("Support file already created so skipping; did you make all of them?")

        plothovs_imos(outfol,'u',plotfol)
        plothovs_imos(outfol,'gsla',plotfol)
        plothovs_imos(outfol,'v',plotfol)

        plothovs_imos(outfol,'eke',plotfol)
        plothovs_imos(outfol,'ekemke',plotfol)
        plothovs_imos(outfol,'mkeeke',plotfol)
    else:
        lg.error("I don't know what to do here!")
        sys.exit()

    lg.info('')
    localtime = time.asctime( time.localtime(time.time()) )
    lg.info("Local current time : "+ str(localtime))
    lg.info('SCRIPT ended')
