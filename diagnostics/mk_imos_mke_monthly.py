# coding=utf-8
#!/usr/bin/env python 
#   Author: Christopher Bull. 
#   Affiliation: Climate Change Research Centre and ARC Centre of Excellence for Climate System Science.
#                Level 4, Mathews Building
#                University of New South Wales
#                Sydney, NSW, Australia, 2052
#   Contact: z3457920@unsw.edu.au
#   www:     christopherbull.com.au
#   Date created: Mon, 13 Feb 2017 17:52:09
#   Machine created on: chris-VirtualBox2
#
"""
This file is designed to create a monthly netCDF file of MKE

"""
import logging as lg
import time
import os

import sys

pathfile = os.path.dirname(os.path.realpath(__file__)) 
sys.path.insert(1,os.path.dirname(pathfile)+'/')

from cb2logger import *

import shareme as sm

import inputdirs as ind

from netCDF4 import Dataset
import numpy as np
import pandas as pd
import collections
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import gridspec
from matplotlib.ticker import MultipleLocator

#import imgtrkr as it
import os
import datetime
import socket
import inspect

import itertools


import collections
#import vorticity
import glob
import shareme as sm

import xarray as xr

import imgtrkr as it
import os
import datetime
import socket
import inspect

from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.ticker import MultipleLocator

if __name__ == "__main__":
    LogStart('',fout=False)
    ##########
    #  init  #
    ##########
    

    workdir=\
    ind.workdir

    imoseke=\
    ind.workdir+'imos_eke/'

    outfol=workdir+'imos_eke/'

    plotfol=ind.plotdir
    sm.mkdir(plotfol)
    sm.mkdir(outfol)

    #################
    #  The Work...  #
    #################

    mkemonthly=imoseke+'monthly_imos_mke'
    if not os.path.exists(mkemonthly+'.npy'):

        dfekeidx=sm.construct_imos_index()
        dfekeidx['month']=dfekeidx.index.month
        months=[]
        for mo in np.arange(12)+1:
            print mo
            gp=dfekeidx[dfekeidx['month']==mo]
            mosu=[]
            mosv=[]

            for date,row in gp.iterrows():
                ifile=Dataset(row['file'], 'r')

                mosu.append(ifile.variables['UCUR'][row['tidx'],:])
                mosv.append(ifile.variables['VCUR'][row['tidx'],:])

            ubar=np.mean(mosu,axis=0)
            vbar=np.mean(mosv,axis=0)

            mke=0.5*(\
            np.square(ubar)+\
            np.square(vbar)\
                    )
            months.append(mke)

        np.save(mkemonthly,months)
        lg.info("File saved: "+ mkemonthly+'.npy')

    mkemo=np.load(mkemonthly+'.npy')
    lg.info("File loaded: "+ mkemonthly+'.npy')
    
    # levs=np.linspace(0,0.15,30)
    # plt.close('all')
    # row=4
    # col=3
    # fig, axis = plt.subplots(\
        # nrows=row, ncols=col, sharex=False, sharey=False,figsize=(5.5*col,20),\
        # gridspec_kw={'hspace':.05,'wspace':.065}\
              # )
    
    # meshf=sm.get_imos_time_mean()
    # meshfile=xr.open_dataset(meshf)
    # lons=meshfile['LONGITUDE'].values
    # lats=meshfile['LATITUDE'].values
    # for idx,ax in zip(np.roll(np.arange(12),1),np.reshape(axis,12)):
        # #ax=axis[0]
        # ax.set_xlim([149,181])
        # ax.set_ylim([-49,-15])
        # cs1=ax.contourf(lons,lats,mkemo[idx],levels=levs,extend='max',cmap='jet')
        # sm.change_tick_labels_add_dirs(ax)
        # if idx+1 not in [12,3,6,9]:
            # plt.setp(ax.get_yticklabels(),visible=False)

        # if idx+1 not in [9,10,11]:
            # plt.setp(ax.get_xticklabels(),visible=False)
        # sm.pl_inset_title_box(ax,str(idx+1),bwidth="10%",location=1)
        # sm.p_imoslmask(meshf,ax)
    # fig.colorbar(cs1, ax=axis.ravel().tolist(), pad=0.02, aspect = 30)
    # #plt.show()
    
    # efile=plotfol+'mke_monthly_imos.png'
    # fig.savefig(efile,dpi=300,bbox_inches='tight')
    # it.AddTrkr(efile,{'Created with':os.path.realpath(__file__),'time':datetime.datetime.now().strftime("%Y-%m-%d %H:%M"),'machine':socket.gethostname(),'function_name':inspect.currentframe().f_code.co_name})
    # print efile


    lg.info("Plotting bias...")
    # now doing BIAS plot with MKE_time-mean removed...
    timemeanf=sm.get_imos_time_mean()
    timemeanf_f=Dataset(timemeanf, 'r')

    # print timemeanf_f.variables.keys()
    mke_all=0.5*(\
    np.square(timemeanf_f.variables['UCUR'][0,:])+\
    np.square(timemeanf_f.variables['VCUR'][0,:])\
            )

    levs=np.linspace(-0.15,0.15,30)
    plt.close('all')
    row=4
    col=3
    fig, axis = plt.subplots(\
        nrows=row, ncols=col, sharex=False, sharey=False,figsize=(5.5*col,20),\
        gridspec_kw={'hspace':.05,'wspace':.065}\
              )
    
    meshf=sm.get_imos_time_mean()
    meshfile=xr.open_dataset(meshf)
    lons=meshfile['LONGITUDE'].values
    lats=meshfile['LATITUDE'].values
    for idx,ax in zip(np.roll(np.arange(12),1),np.reshape(axis,12)):
        #ax=axis[0]
        cs1=ax.contourf(lons,lats,mkemo[idx]-mke_all,levels=levs,extend='both',cmap='seismic')
        ax.set_xlim([144,161])
        ax.set_ylim([-45,-10])
        sm.change_tick_labels_add_dirs(ax)
        if idx+1 not in [12,3,6,9]:
            plt.setp(ax.get_yticklabels(),visible=False)

        if idx+1 not in [9,10,11]:
            plt.setp(ax.get_xticklabels(),visible=False)
        sm.pl_inset_title_box(ax,str(idx+1),bwidth="10%",location=1)
        sm.p_imoslmask(meshf,ax)
    fig.colorbar(cs1, ax=axis.ravel().tolist(), pad=0.02, aspect = 30)
    #plt.show()
    
    efile=plotfol+'mke_monthly_imos_MKEremoved_bias.png'
    fig.savefig(efile,dpi=300,bbox_inches='tight')
    it.AddTrkr(efile,{'Created with':os.path.realpath(__file__),'time':datetime.datetime.now().strftime("%Y-%m-%d %H:%M"),'machine':socket.gethostname(),'function_name':inspect.currentframe().f_code.co_name})
    print efile

    lg.info('')
    #localtime = time.asctime( time.localtime(time.time()) )
    #lg.info("Local current time : "+ str(localtime))
    lg.info('SCRIPT ended')
