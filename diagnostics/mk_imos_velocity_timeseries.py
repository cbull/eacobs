# coding=utf-8
#!/usr/bin/env python 
#   Author: Christopher Bull. 
#   Affiliation: Climate Change Research Centre and ARC Centre of Excellence for Climate System Science.
#                Level 4, Mathews Building
#                University of New South Wales
#                Sydney, NSW, Australia, 2052
#   Contact: z3457920@unsw.edu.au
#   www:     christopherbull.com.au
#   Date created: Mon, 13 Feb 2017 17:52:09
#   Machine created on: chris-VirtualBox2
#
"""
This file is designed to extract a timeseries of surface V velocity across a section of imos

"""
import logging as lg
import time
import os

import sys

pathfile = os.path.dirname(os.path.realpath(__file__)) 
sys.path.insert(1,os.path.dirname(pathfile)+'/')

from cb2logger import *

import shareme as sm

import inputdirs as ind


from netCDF4 import Dataset
import xarray as xr
import numpy as np
import pandas as pd
import collections
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import gridspec
from matplotlib.ticker import MultipleLocator

from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.ticker import MultipleLocator

#import imgtrkr as it
import os
import datetime
import socket
import inspect

import itertools

from mpl_toolkits.axes_grid.inset_locator import inset_axes

import collections
#import vorticity
import glob
import shareme as sm


def extract_timeseries(dfidx,sections,efile):
    """function to extract time series and create hdfstore
    :returns: @todo
    """
    
    secs=collections.OrderedDict()
    for section in sections:
        #print section
        ifile=Dataset(dfidx.iloc[0]['file'], 'r')
        
        #startlat=sm.find_nearest(ifile.variables['LATITUDE'][:],-27)
        #endlat=sm.find_nearest(ifile.variables['LATITUDE'][:],-37.5)
        lat=sm.find_nearest(ifile.variables['LATITUDE'][:],ind.sectiondefs[section[0]][1])
        slong=sm.find_nearest(ifile.variables['LONGITUDE'][:],ind.sectiondefs[section[0]][0])
        endlong=sm.find_nearest(ifile.variables['LONGITUDE'][:],ind.sectiondefs[section[1]][0])
        ifile.close()

        vel=collections.OrderedDict()
        #cnt=0
        for date,row in dfidx.iterrows():
            #print date
            lg.info("Section name: "+ section +" . Date "+os.path.basename(row['file']))
            ifile=Dataset(row['file'], 'r')
            vel[date]=np.mean(ifile.variables['VCUR'][row['tidx'],lat,slong:endlong])
            #cnt+=1
            #if cnt==700:
                #break
        secs[section]=vel
    df=pd.DataFrame(secs)

    #a clobber check here
    try:
        os.remove(efile)
        lg.info("HDFStore already existed, clobbering!")
    except OSError:
        pass 
    
    #Due to Pandas 'TypeError' had to use put rather than append...
    store = pd.HDFStore(efile,complevel=9, complib='blosc')
    store.put('df',df)
    store.close()

    lg.info("HDFStore created: " + efile)
    return


if __name__ == "__main__":                                     #are we being run directly?
    LogStart('',fout=False)
    ###########################################################################
    #                                  init                                   #
    ###########################################################################

    imosfol=\
    ind.imosfol
    #'/srv/ccrc/data49/z3457920/RawData/IMOS/GSLA/'

    workdir=\
    ind.workdir
    #'/srv/ccrc/data49/z3457920/20170211_EACwarmingIMOS/'

    plotfol=ind.plotdir
    sm.mkdir(plotfol)
    sm.mkdir(workdir)

    timemeanf=sm.get_imos_time_mean()

    ###########################################################################
    #                                  main                                   #
    ###########################################################################

    if ind.paper_case=='20170211_EACwarmingIMOS':
        dfidx=sm.construct_imos_index()

        ofile=workdir+'velocity_timeseries_imos_table.h5'
        if not os.path.exists(ofile):
            lg.info("Creating support files: " + ofile)
            extract_timeseries(dfidx,['AB','FE','JK','LM'],ofile)
        else:
            lg.warning("Support file: "+ofile + " already created so skipping")

    else:
        lg.error("I don't know what to do here!")
        sys.exit()

    lg.info('')
    localtime = time.asctime( time.localtime(time.time()) )
    lg.info("Local current time : "+ str(localtime))
    lg.info('SCRIPT ended')
