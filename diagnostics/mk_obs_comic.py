# coding=utf-8
#!/usr/bin/env python 
#   Author: Christopher Bull. 
#   Affiliation: Climate Change Research Centre and ARC Centre of Excellence for Climate System Science.
#                Level 4, Mathews Building
#                University of New South Wales
#                Sydney, NSW, Australia, 2052
#   Contact: z3457920@unsw.edu.au
#   www:     christopherbull.com.au
#   Date created: Mon, 13 Feb 2017 17:52:09
#   Machine created on: chris-VirtualBox2
#
"""
This file is designed to make the intro comic for the EAC obs paper.

Notes: 
    Need to have run mk_imos_hovmoller.py
"""
import logging as lg
import time
import os

import sys

pathfile = os.path.dirname(os.path.realpath(__file__)) 
sys.path.insert(1,os.path.dirname(pathfile)+'/')

from cb2logger import *

import shareme as sm

import inputdirs as ind


from netCDF4 import Dataset
import xarray as xr
import numpy as np
import pandas as pd
import collections
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import gridspec
from matplotlib.ticker import MultipleLocator

from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.ticker import MultipleLocator

import imgtrkr as it
import os
import datetime
import socket
import inspect

import itertools

from mpl_toolkits.axes_grid.inset_locator import inset_axes

import collections
#import vorticity
import glob
import shareme as sm


class PVect(object):
    """
    Class to create AVISO vector velocity plot.

    Parameters
    ----------
    ufile,vfile,tfile: 

    Returns
    -------
    
    Notes
    -------
    

    Example
    --------
    """
    def __init__(self, infile):
        # super(PVect, self).__init__()
        self.infile=infile

        self.arrayx,self.arrayy=self.fdata()

        
    def fdata(self):
        """function to get data for a pretty plot for the circulation in the tasman sea
        
        :arg1: @todo
        :returns: @todo
        """

        self.ifile=Dataset(self.infile, 'r')
        ufield=self.ifile.variables['UCUR'][0,:]
        vfield=self.ifile.variables['VCUR'][0,:]
        #ifile.close()
        return ufield,vfield

    def main_plot_vects(self,pts,outpufol,regions=False):
        """function to make a pretty plot for the circulation in the tasman sea
        
        :arg1: @todo
        :returns: @todo
        """
        import matplotlib.ticker as ticker
        def fmt(x, pos):
            """
            Ripped from: 
            http://stackoverflow.com/questions/25983218/scientific-notation-colorbar-in-matplotlib
            """
            return '{:.1f}'.format(x)


        #assert(os.path.exists(pts)),"Can't find your HDFStore"
        df_coords=pd.HDFStore(pts).df

        plt.close('all')
        row=1
        col=2
        fig, axis = plt.subplots(\
            nrows=row, ncols=col, sharex=False, sharey=False,figsize=(5.5*(col-1),4.5),\
            gridspec_kw={'hspace':.225,'wspace':.065,'width_ratios':[1]*(col-1)+[0.04]}\
                      )

        ax=axis[0]

        #assert(os.path.exists(self.tfile)),"netCDF file does not exist!"
        #ifile=Dataset(self.tfile, 'r')
        #lats=ifile.variables['nav_lat'][:]
        #lons=sm.nemo_fixdateline(ifile)
        lons=self.ifile.variables['LONGITUDE'][:]
        lats=self.ifile.variables['LATITUDE'][:]

        sm.p_imoslmask(self.infile,ax)
        x,y=np.meshgrid(lons,lats)
        mag=np.sqrt(np.square(self.arrayx)+np.square(self.arrayy))
        magc=ax.contourf(lons,lats,mag,levels=np.linspace(0,0.5,20),cmap='Blues_r',extend='max',alpha=0.9)

        skip=(slice(None,None,3),slice(None,None,3))
        Q=ax.quiver(x[skip], y[skip], self.arrayx[skip], self.arrayy[skip], pivot='mid',color='black', units='xy',headwidth=4,scale=.3,  headlength=2,angles='xy',scale_units='xy',alpha=0.7, headaxislength=3)

        sm.pl_inset_title_box(ax,'time-mean EAC surface velocity',bwidth="60%",location=4)


        x=df_coords.lat
        globalymin=-27
        minix=df_coords.lat.iloc[sm.find_nearest(x,globalymin):].values.tolist()
        miniy=df_coords.lon.iloc[sm.find_nearest(x,globalymin):].values.tolist()
        for lon,lat in zip(miniy,minix):
            ax.scatter(lon,lat,marker='.',s=20,color='r',lw=1,alpha=0.4)

        if regions:
            sm.fill_box(ax,'EACxOne')
            sm.fill_box(ax,'EACxTwo')
            sm.fill_box(ax,'EACUPOne')
            sm.fill_box(ax,'TFone')
            sm.fill_box(ax,'EACmain')
            sm.fill_box(ax,'SECC')
            sm.fill_box(ax,'TFone')
            sm.fill_box(ax,'NZone')
            sm.draw_section(ax,'FE','zonal')
            sm.draw_section(ax,'AB','zonal')
            sm.draw_section(ax,'JK','zonal')
            # sm.draw_section(ax,'LM','zonal')

        ax.set_xlim([141,185])
        ax.set_ylim([-51,-18])
        sm.change_tick_labels_add_dirs(ax)

        qk=ax.quiverkey(Q, 0.9, 1.05, 1, r'$1 \frac{m}{s}$',
                           labelpos='E',
                        fontproperties={'weight': 'bold'})
        qk.text.set_backgroundcolor('w')

        ax=axis[1]
        cb1=plt.colorbar(magc,cax=ax,orientation='vertical',format=ticker.FuncFormatter(fmt))
        cb1.set_label("Mean velocity magnitude")

        #plt.show()
        efile=outpufol+'slicelocations.png'
        fig.savefig(efile,dpi=300)
        #fig.savefig(efile[:-4]+'.pdf',format='pdf')
        
        #it.AddTrkr(efile,{'Created with':os.path.realpath(__file__),'time':datetime.datetime.now().strftime("%Y-%m-%d %H:%M"),'machine':socket.gethostname(),'function_name':inspect.currentframe().f_code.co_name})
        # plt.show()
        print efile
        return

if __name__ == "__main__":                                     #are we being run directly?
    LogStart('',fout=False)
    ###########################################################################
    #                                  init                                   #
    ###########################################################################

    imosfol=\
    ind.imosfol
    #'/srv/ccrc/data49/z3457920/RawData/IMOS/GSLA/'

    workdir=\
    ind.workdir
    #'/srv/ccrc/data49/z3457920/20170211_EACwarmingIMOS/'

    timemeanf=sm.get_imos_time_mean()

    plotfol=ind.plotdir
    sm.mkdir(plotfol)

    ###########################################################################
    #                                  main                                   #
    ###########################################################################
    
    if ind.paper_case=='20170211_EACwarmingIMOS':
        plotone=PVect(timemeanf)
        plotone.main_plot_vects(workdir+'hovmollers/'+'imos_output'+'_u_table'+ '.h5',plotfol,regions=True)

        #toy sections...
        #dfidx=sm.construct_imos_index()

        #plt.close('all')
        #row=1
        #col=2
        #fig, axis = plt.subplots(\
            #nrows=row, ncols=col, sharex=False, sharey=False,figsize=(5.5*(col-1),4.5),\
            #gridspec_kw={'hspace':.225,'wspace':.065,'width_ratios':[1]*(col-1)+[0.04]}\
                      #)

        #ax=axis[0]

        #ifile=Dataset(dfidx.iloc[0]['file'], 'r')
        #lons=ifile.variables['LONGITUDE'][:]
        #lats=ifile.variables['LATITUDE'][:]

        #sm.p_imoslmask(dfidx.iloc[0]['file'],ax)

        #sm.fill_box(ax,'EACxOne')
        #sm.fill_box(ax,'EACxTwo')
        #sm.fill_box(ax,'TFone')
        #sm.draw_section(ax,'FE','zonal')
        #sm.draw_section(ax,'AB','zonal')
        #sm.draw_section(ax,'JK','zonal')
        #ax.set_xlim([141,178])
        #ax.set_ylim([-45,-27])
        #plt.show()

    else:
        lg.error("I don't know what to do here!")
        sys.exit()
    lg.info('')
    localtime = time.asctime( time.localtime(time.time()) )
    lg.info("Local current time : "+ str(localtime))
    lg.info('SCRIPT ended')
