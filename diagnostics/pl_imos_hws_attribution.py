#!/usr/bin/env python 
#   Author: Christopher Bull. 
#   Affiliation: Climate Change Research Centre and ARC Centre of Excellence for Climate System Science.
#                Level 4, Mathews Building
#                University of New South Wales
#                Sydney, NSW, Australia, 2052
#   Contact: z3457920@unsw.edu.au
#   www:     christopherbull.com.au
#   Date created: Mon, 20 Feb 2017 20:03:28
#   Machine created on: chris-VirtualBox2
#

"""
This file is try and attribute the heatwaves to upstream EAC advection.

Relies on:
    /home/z3457920/hdrive/repos/eacobs/diagnostics/find_heatwaves.py

"""
import logging as lg
import time
import os

import sys

pathfile = os.path.dirname(os.path.realpath(__file__)) 
sys.path.insert(1,os.path.dirname(pathfile)+'/')

from cb2logger import *

import shareme as sm

import inputdirs as ind

from netCDF4 import Dataset
import numpy as np
import pandas as pd
import collections
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import gridspec
from matplotlib.ticker import MultipleLocator

from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.ticker import MultipleLocator

import imgtrkr as it
import os
import datetime
import socket
import inspect

import itertools

from mpl_toolkits.axes_grid.inset_locator import inset_axes

import collections
#import vorticity
import glob
import shareme as sm


from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.ticker import MultipleLocator

import matplotlib.ticker as ticker

class PVectHW(object):
    """
    Class to create IMOS vector velocity plot.

    Parameters
    ----------
    ufile,vfile,tfile: 

    Returns
    -------
    
    Notes
    -------
    

    Example
    --------
    """
    def __init__(self, infile,tstep,pltaxis):
        self.infile=infile
        self.tstep=tstep
        self.pltaxis=pltaxis

        self.arrayx,self.arrayy=self.fdata()

        
    def fdata(self):
        """function to get data for a pretty plot for the circulation in the tasman sea
        
        :arg1: @todo
        :returns: @todo
        """

        self.ifile=Dataset(self.infile, 'r')
        ufield=self.ifile.variables['UCUR'][self.tstep,:]
        vfield=self.ifile.variables['VCUR'][self.tstep,:]
        #ifile.close()
        return ufield,vfield

    def main_plot_vects(self,titbox,regions=''):
        """function to make a pretty plot for the circulation in the tasman sea
        
        :arg1: @todo
        :returns: @todo
        """


        ax=self.pltaxis

        lons=self.ifile.variables['LONGITUDE'][:]
        lats=self.ifile.variables['LATITUDE'][:]

        sm.p_imoslmask(self.infile,ax)
        x,y=np.meshgrid(lons,lats)
        mag=np.sqrt(np.square(self.arrayx)+np.square(self.arrayy))
        magc=ax.contourf(lons,lats,mag,levels=np.linspace(0,0.5,20),cmap='Blues_r',extend='max',alpha=0.9)

        skip=(slice(None,None,3),slice(None,None,3))
        Q=ax.quiver(x[skip], y[skip], self.arrayx[skip], self.arrayy[skip], pivot='mid',color='black', units='xy',headwidth=4,scale=.3,  headlength=2,angles='xy',scale_units='xy',alpha=0.7, headaxislength=3)

        sm.pl_inset_title_box(ax,titbox,bwidth="40%",location=4)

        if regions!='':
            sm.fill_box(ax,regions)

        ax.set_xlim([143,169])
        ax.set_ylim([-47,-20])
        sm.change_tick_labels_add_dirs(ax)

        qk=ax.quiverkey(Q, 0.9, 1.05, 1, r'$1 \frac{m}{s}$',
                           labelpos='E',
                        fontproperties={'weight': 'bold'})
        qk.text.set_backgroundcolor('w')

        return


class PVectHWmean(object):
    """
    Class to create IMOS vector velocity plot.

    Parameters
    ----------
    ufile,vfile,tfile: 

    Returns
    -------
    
    Notes
    -------
    

    Example
    --------
    """
    def __init__(self, infiles,tsteps,pltaxis):
        self.infiles=infiles
        self.tsteps=tsteps
        self.pltaxis=pltaxis
        self.arrayx,self.arrayy=self.fdata()
        
    def fdata(self):
        """function to get data for a pretty plot for the circulation in the tasman sea
        
        :arg1: @todo
        :returns: @todo
        """

        u=[]
        v=[]
        for f,idx in zip(self.infiles,self.tsteps):
            ifile=Dataset(f, 'r')
            u.append(ifile.variables['UCUR'][idx,:])
            v.append(ifile.variables['VCUR'][idx,:])
            ifile.close()
        
        getm=Dataset(self.infiles[0],'r').variables['UCUR'][0,:]
        umean=np.ma.masked_where(getm.filled()==1e+20,np.mean(u,axis=0)) 
        vmean=np.ma.masked_where(getm.filled()==1e+20,np.mean(v,axis=0)) 
        return umean,vmean

    def main_plot_vects(self,lons,lats,titbox,regions=''):
        """function to make a pretty plot for the circulation in the tasman sea
        
        :arg1: @todo
        :returns: @todo
        """

        ax=self.pltaxis

        sm.p_imoslmask(self.infiles[0],ax)
        x,y=np.meshgrid(lons,lats)
        mag=np.sqrt(np.square(self.arrayx)+np.square(self.arrayy))
        magc=ax.contourf(lons,lats,mag,levels=np.linspace(0,0.5,20),cmap='Blues_r',extend='max',alpha=0.9)

        skip=(slice(None,None,3),slice(None,None,3))
        Q=ax.quiver(x[skip], y[skip], self.arrayx[skip], self.arrayy[skip], pivot='mid',color='black', units='xy',headwidth=4,scale=.3,  headlength=2,angles='xy',scale_units='xy',alpha=0.7, headaxislength=3)

        sm.pl_inset_title_box(ax,titbox,bwidth="70%",location=4)

        if regions!='':
            sm.fill_box(ax,regions)

        ax.set_xlim([143,169])
        ax.set_ylim([-47,-20])
        sm.change_tick_labels_add_dirs(ax)

        qk=ax.quiverkey(Q, 0.9, 1.05, 1, r'$1 \frac{m}{s}$',
                           labelpos='E',
                        fontproperties={'weight': 'bold'})
        qk.text.set_backgroundcolor('w')
        # plt.show()

        return

class PVectSSTAHWmean(object):
    """
    Class to create IMOS vector velocity and OISSTA plot.

    Parameters
    ----------
    ufile,vfile,tfile: 

    Returns
    -------
    
    Notes
    -------
    

    Example
    --------
    """
    def __init__(self, infiles,tsteps,infiles_oi,tsteps_oi,pltaxis):
        self.infiles=infiles
        self.tsteps=tsteps

        self.infiles_oi=infiles_oi
        self.tsteps_oi=tsteps_oi
        self.pltaxis=pltaxis
        self.arrayx,self.arrayy=self.fdata_vec()
        self.array_oi=self.fdata_oissta()
        
    def fdata_vec(self):
        """function to get data for a pretty plot for the circulation in the tasman sea
        
        :arg1: @todo
        :returns: @todo
        """

        u=[]
        v=[]
        for f,idx in zip(self.infiles,self.tsteps):
            ifile=Dataset(f, 'r')
            u.append(ifile.variables['UCUR'][idx,:])
            v.append(ifile.variables['VCUR'][idx,:])
            ifile.close()
        
        getm=Dataset(self.infiles[0],'r').variables['UCUR'][0,:]
        umean=np.ma.masked_where(getm.filled()==1e+20,np.mean(u,axis=0)) 
        vmean=np.ma.masked_where(getm.filled()==1e+20,np.mean(v,axis=0)) 
        return umean,vmean

    def fdata_oissta(self):
        """function to get data for a pretty plot for the circulation in the tasman sea
        
        :arg1: @todo
        :returns: @todo
        """

        ssta=[]
        for f,idx in zip(self.infiles_oi,self.tsteps_oi):
            # print f
            ifile=Dataset(f, 'r')
            ssta.append(ifile.variables['anom'][idx,0,:])
            ifile.close()
        
        getm=Dataset(self.infiles_oi[0],'r').variables['anom'][idx,0,:]
        ssta_mean=np.ma.masked_where(getm.filled()==1e+20,np.mean(ssta,axis=0)) 
        return ssta_mean

    def main_plot_vects(self,lons,lats,lons_oisst,lats_oisst,titbox,regions=''):
        """function to make a pretty plot for the circulation in the tasman sea
        
        :arg1: @todo
        :returns: @todo
        """

        ax=self.pltaxis

        sm.p_imoslmask(self.infiles[0],ax)
        x,y=np.meshgrid(lons,lats)

        xx,yy=np.meshgrid(lons_oisst,lats_oisst)
        self.magc=ax.contourf(yy,xx,self.array_oi.T,levels=np.linspace(-5,5,15),cmap='seismic',extend='both',alpha=0.6)
        skip=(slice(None,None,3),slice(None,None,3))
        Q=ax.quiver(x[skip], y[skip], self.arrayx[skip], self.arrayy[skip], pivot='mid',color='black', units='xy',headwidth=4,scale=.3,  headlength=2,angles='xy',scale_units='xy',alpha=0.8, headaxislength=3)

        sm.pl_inset_title_box(ax,titbox,bwidth="70%",location=4)

        if regions!='':
            sm.fill_box(ax,regions)

        ax.set_xlim([143,169])
        ax.set_ylim([-47,-20])
        sm.change_tick_labels_add_dirs(ax)
        # plt.show()

        return


def nearest(items, pivot):
    """
    ripped: http://stackoverflow.com/questions/32237862/find-the-closest-date-to-a-given-date
    """
    return min(items, key=lambda x: abs(x - pivot))

def plot_imos_main_one_velocityonly(dataframe,dfimos,bname,pfol):
    """@todo: Docstring for plot_imos_main_one
    
    :dataframe: @todo
    :returns: @todo
    """
    lg.info("Plotting heatwave attribution " + bname)

    dates=collections.OrderedDict()
    dates['t0']=[]
    dates['t50']=[]
    dates['t100']=[]
    for idx,row in dataframe.iterrows():
        mid=row['start'] + (row['end'] - row['start'])/2
        imos_mid=nearest(dfimos.index,mid)
        dates['t0'].append(dfimos.ix[imos_mid])
        dates['t50'].append(dfimos.ix[nearest(dfimos.index,imos_mid-pd.Timedelta('50 days'))])
        dates['t100'].append(dfimos.ix[nearest(dfimos.index,imos_mid-pd.Timedelta('100 days'))])

    df=pd.DataFrame(dates)
    plt.close('all')
    row=5
    col=3
    fig, axis = plt.subplots(\
        nrows=row, ncols=col, sharex=False, sharey=False,figsize=(5.5*col,5*row),\
        gridspec_kw={'hspace':.125,'wspace':.065}\
              )
    
    roww=0
    for idx,row in df.iloc[0:5].iterrows():
        for coll,tminus in enumerate(['t0','t50','t100']):
            lg.debug("Currently plotting %s %s %s %s",str(idx),str(roww),str(coll),str(tminus))
            ax=axis[roww,coll]
            p=PVectHW(row.ix[tminus]['file'],row.ix[tminus]['tidx'],ax)
            p.main_plot_vects(str(row.ix[tminus]['date'])[0:10],regions=bname)

            if roww==0:
                if coll==0:
                    ax.set_title('Event Peak')
                elif coll==1:
                    ax.set_title('50 days prior')
                elif coll==2:
                    ax.set_title('100 days prior')

            if roww!=4:
                plt.setp(ax.get_xticklabels(),visible=False)
            if coll!=0:
                plt.setp(ax.get_yticklabels(),visible=False)
        roww+=1
    #plt.show()
    #import ipdb; ipdb.set_trace()

    #plt.show()
    efile=pfol+bname+'_attr_iCum_'+'.png'
    lg.info("Sooo many vectors, takes a few min to save.." + efile)
    fig.savefig(efile,dpi=300,bbox_inches='tight')
    #fig.savefig(efile,dpi=300)
    
    it.AddTrkr(efile,{'Created with':os.path.realpath(__file__),'time':datetime.datetime.now().strftime("%Y-%m-%d %H:%M"),'machine':socket.gethostname(),'function_name':inspect.currentframe().f_code.co_name})
    print efile
    return


def plot_imos_main_one_velocitymean(dataframe,dfimos,bname,pfol):
    """@todo: Docstring for plot_imos_main_one
    
    :dataframe: @todo
    :returns: @todo
    """
    lg.info("Plotting heatwave attribution " + bname)

    dates=collections.OrderedDict()
    dates['t0']=[]
    dates['t30']=[]
    dates['t60']=[]
    for idx,row in dataframe.iterrows():
        mid=row['start'] + (row['end'] - row['start'])/2
        imos_mid=nearest(dfimos.index,mid)
        dates['t0'].append(dfimos.ix[imos_mid])
        dates['t30'].append(dfimos.ix[nearest(dfimos.index,imos_mid-pd.Timedelta('30 days'))])
        dates['t60'].append(dfimos.ix[nearest(dfimos.index,imos_mid-pd.Timedelta('60 days'))])

    df=pd.DataFrame(dates)
    plt.close('all')
    row=5
    col=3
    fig, axis = plt.subplots(\
        nrows=row, ncols=col, sharex=False, sharey=False,figsize=(5.5*col,5*row),\
        gridspec_kw={'hspace':.125,'wspace':.065}\
              )

    tfile=Dataset(dfimos.iloc[0]['file'], 'r')
    lons=tfile.variables['LONGITUDE'][:]
    lats=tfile.variables['LATITUDE'][:]
    tfile.close()
    
    roww=0
    for idx,row in df.iloc[0:5].iterrows():
        for coll,tminus in enumerate(['t0','t30','t60']):
            lg.debug("Currently plotting %s %s %s %s",str(idx),str(roww),str(coll),str(tminus))
            ax=axis[roww,coll]

            cur_idx=dfimos.index.get_loc(row.ix[tminus]['date'])

            dfslice=dfimos.ix[cur_idx-9:cur_idx+1]

            #on the unusual instance where it is at the start of the timeseries...
            if len(dfslice)==0 and cur_idx==0: 
                sindf=dfimos.ix[row.ix[tminus]['date']]
                
                # print str(sindf['date'])[0:10]
                p=PVectHWmean([sindf['file']],[sindf['tidx']],ax)
                p.main_plot_vects(\
                        lons,lats,\
                        str(sindf['date'])[0:10],\
                        regions=bname)
            else:
                # print 'moo',str(dfslice['date'][0])[0:10]+' - '+str(row.ix[tminus]['date'])[0:10]
                p=PVectHWmean(dfslice['file'].values.tolist(),dfslice['tidx'].values.tolist(),ax)
                p.main_plot_vects(\
                        lons,lats,\
                        str(dfslice['date'][0])[0:10]+' - '+str(row.ix[tminus]['date'])[0:10],\
                        regions=bname)
            

            if roww==0:
                if coll==0:
                    ax.set_title('Event Peak')
                elif coll==1:
                    ax.set_title('30 days prior')
                elif coll==2:
                    ax.set_title('60 days prior')

            if roww!=4:
                plt.setp(ax.get_xticklabels(),visible=False)
            if coll!=0:
                plt.setp(ax.get_yticklabels(),visible=False)
        roww+=1
    #plt.show()
    #import ipdb; ipdb.set_trace()

    #plt.show()
    efile=pfol+bname+'_attr_iCum_mean10'+'.png'
    lg.info("Sooo many vectors, takes a few min to save.." + efile)
    fig.savefig(efile,dpi=300,bbox_inches='tight')
    #fig.savefig(efile,dpi=300)
    
    it.AddTrkr(efile,{'Created with':os.path.realpath(__file__),'time':datetime.datetime.now().strftime("%Y-%m-%d %H:%M"),'machine':socket.gethostname(),'function_name':inspect.currentframe().f_code.co_name})
    print efile
    return

def fmt(x, pos):
    """
    Ripped from: 
    http://stackoverflow.com/questions/25983218/scientific-notation-colorbar-in-matplotlib
    """
    return '{:.1f}'.format(x)

def plot_imos_main_one_oisstmean(dataframe,dfimos,dfoisst,bname,pfol):
    """@todo: Docstring for plot_imos_main_one
    
    :dataframe: @todo
    :returns: @todo
    """
    lg.info("Plotting heatwave attribution " + bname)

    dates=collections.OrderedDict()
    dates['t0']=[]
    dates['t30']=[]
    dates['t60']=[]

    dates_oisst=collections.OrderedDict()

    dates_oisst['t0']=[]
    dates_oisst['t30']=[]
    dates_oisst['t60']=[]
    for idx,row in dataframe.iterrows():
        mid=row['start'] + (row['end'] - row['start'])/2
        imos_mid=nearest(dfimos.index,mid)
        dates['t0'].append(dfimos.ix[imos_mid])
        dates['t30'].append(dfimos.ix[nearest(dfimos.index,imos_mid-pd.Timedelta('30 days'))])
        dates['t60'].append(dfimos.ix[nearest(dfimos.index,imos_mid-pd.Timedelta('60 days'))])

        imos_mid=nearest(dfoisst.index,mid)
        dates_oisst['t0'].append(dfoisst.ix[imos_mid])
        dates_oisst['t30'].append(dfoisst.ix[nearest(dfoisst.index,imos_mid-pd.Timedelta('30 days'))])
        dates_oisst['t60'].append(dfoisst.ix[nearest(dfoisst.index,imos_mid-pd.Timedelta('60 days'))])

    df=pd.DataFrame(dates)
    df_oisst=pd.DataFrame(dates_oisst)
    plt.close('all')
    row=5
    col=3
    fig, axis = plt.subplots(\
        nrows=row, ncols=col, sharex=False, sharey=False,figsize=(5.5*col,5*row),\
        gridspec_kw={'hspace':.075,'wspace':.065}\
              )

    tfile=Dataset(dfimos.iloc[0]['file'], 'r')
    lons=tfile.variables['LONGITUDE'][:]
    lats=tfile.variables['LATITUDE'][:]
    tfile.close()

    tfile_oisst=Dataset(dfoisst.iloc[0]['file'], 'r')
    lons_oi=tfile_oisst.variables['lat'][:]
    lats_oi=tfile_oisst.variables['lon'][:]
    tfile_oisst.close()
    
    roww=0
    for idx,row in df.iloc[0:5].iterrows():
        for coll,tminus in enumerate(['t0','t30','t60']):
            lg.debug("Currently plotting %s %s %s %s",str(idx),str(roww),str(coll),str(tminus))
            ax=axis[roww,coll]

            cur_idx=dfimos.index.get_loc(row.ix[tminus]['date'])
            dfslice=dfimos.ix[cur_idx-9:cur_idx+1]

            row_oi=df_oisst.iloc[idx]
            cur_idx_oi=dfoisst.index.get_loc(row_oi.ix[tminus]['date'])
            dfslice_oi=dfoisst.ix[cur_idx_oi-9:cur_idx_oi+1]

            #find where the dates overlap..
            int_dates=pd.Series(list(set(dfslice_oi.date.values).intersection(set(dfslice.date.values))))
            if len(int_dates)==0:
                lg.warning("Couldn't find any overlap between dates!")
                lg.warning("----IMOS dates...---")
                lg.warning(str([str(dd)[0:10] for dd in dfslice.date.values]))
                lg.warning("")
                lg.warning("----OISST dates...---")
                lg.warning(str([str(dd)[0:10] for dd in dfslice_oi.date.values]))
                lg.warning("Couldn't find any overlap between dates!")
                continue

            #on the unusual instance where it is at the start of the timeseries...
            if len(dfslice)==0 and cur_idx==0: 
                lg.warning("houston problem!")
                import ipdb
                ipdb.set_trace()
                pass
                # sindf=dfimos.ix[row.ix[tminus]['date']]
                # sindf_oi=dfoisst.ix[row.ix[tminus]['date']]
                
                # # print str(sindf['date'])[0:10]
                # p=PVectHWmean([sindf['file']],[sindf['tidx']],ax)
                # p.main_plot_vects(\
                        # lons,lats,\
                        # str(sindf['date'])[0:10],\
                        # regions=bname)
            else:
                p=PVectSSTAHWmean(\
                dfslice.ix[int_dates]['file'].values.tolist(),\
                dfslice.ix[int_dates]['tidx'].values.tolist(),\
                dfslice_oi.ix[int_dates]['file'].values.tolist(),\
                dfslice_oi.ix[int_dates]['tidx'].values.tolist()\
                ,ax)
                p.main_plot_vects(\
                        lons,lats,lons_oi,lats_oi,\
                        str(int_dates.iloc[0])[0:10]+' - '+str(int_dates.iloc[-1])[0:10],\
                        regions=bname)
            
            if roww==0:
                if coll==0:
                    ax.set_title('Event Peak')
                elif coll==1:
                    ax.set_title('30 days prior')
                elif coll==2:
                    ax.set_title('60 days prior')

            if roww!=4:
                plt.setp(ax.get_xticklabels(),visible=False)
            if coll!=0:
                plt.setp(ax.get_yticklabels(),visible=False)
        roww+=1

    fig.colorbar(p.magc, ax=axis.ravel().tolist(), pad=0.02, aspect =60,format=ticker.FuncFormatter(fmt))

    efile=pfol+bname+'_attr_iCum_mean_10_ssta_vel'+'.png'
    lg.info("Sooo many vectors, takes a few min to save.." + efile)
    fig.savefig(efile,dpi=300,bbox_inches='tight')
    #fig.savefig(efile,dpi=300)
    
    it.AddTrkr(efile,{'Created with':os.path.realpath(__file__),'time':datetime.datetime.now().strftime("%Y-%m-%d %H:%M"),'machine':socket.gethostname(),'function_name':inspect.currentframe().f_code.co_name})
    print efile
    return

if __name__ == "__main__":
    LogStart('',fout=False)
    ##########
    #  init  #
    ##########
    
    oisst=\
    ind.oisst

    workdir=\
    ind.workdir

    plotfol=ind.plotdir
    sm.mkdir(plotfol)

    #################
    #  The Work...  #
    #################

    dfidx=sm.construct_imos_index()
    doisst=sm.construct_oisst_index()
    ifiles=sorted(glob.glob(plotfol + 'hw_*/*.h5' ))
    assert(ifiles!=[]),"glob didn't find anything!"
    for f in ifiles:
        if 'EACxOne' in f:
            df=pd.HDFStore(f).df
            # # plot_imos_main_one_velocityonly(df,dfidx,'EACxOne',plotfol)

            # plot_imos_main_one_velocitymean(df,dfidx,'EACxOne',plotfol)
            plot_imos_main_one_oisstmean(df,dfidx,doisst,'EACxOne',plotfol)

    for f in ifiles:
        if 'EACxTwo' in f:
            df=pd.HDFStore(f).df
            # plot_imos_main_one_velocityonly(df,dfidx,'EACxTwo',plotfol)

            # plot_imos_main_one_velocitymean(df,dfidx,'EACxTwo',plotfol)
            plot_imos_main_one_oisstmean(df,dfidx,doisst,'EACxTwo',plotfol)

    lg.info('')
    localtime = time.asctime( time.localtime(time.time()) )
    lg.info("Local current time : "+ str(localtime))
    lg.info('SCRIPT ended')
