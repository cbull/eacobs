# coding=utf-8
#!/usr/bin/env python 
#   Author: Christopher Bull. 
#   Affiliation: Climate Change Research Centre and ARC Centre of Excellence for Climate System Science.
#                Level 4, Mathews Building
#                University of New South Wales
#                Sydney, NSW, Australia, 2052
#   Contact: z3457920@unsw.edu.au
#   www:     christopherbull.com.au
#   Date created: Mon, 13 Feb 2017 17:52:09
#   Machine created on: chris-VirtualBox2
#
"""
This file is plot a timeseries of EAC velocities and temperature 

Relies on:
    /home/z3457920/hdrive/repos/eacobs/diagnostics/mk_oisst_timeseries.py
    /home/z3457920/hdrive/repos/eacobs/diagnostics/mk_imos_velocity_timeseries.py
    /home/z3457920/hdrive/repos/eacobs/diagnostics/mk_imos_eke_timeseries.py

"""
import logging as lg
import time
import os

import sys

pathfile = os.path.dirname(os.path.realpath(__file__)) 
sys.path.insert(1,os.path.dirname(pathfile)+'/')

from cb2logger import *

import shareme as sm

import inputdirs as ind

from netCDF4 import Dataset
import numpy as np
import pandas as pd
import collections
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import gridspec
from matplotlib.ticker import MultipleLocator

from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.ticker import MultipleLocator

import imgtrkr as it
import os
import datetime
import socket
import inspect

import itertools

from mpl_toolkits.axes_grid.inset_locator import inset_axes

import collections
#import vorticity
import glob
import shareme as sm


from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.ticker import MultipleLocator


def plot_fft(pltaxis,dataframe,co):
    """function to plot section
    
    :section_num: @todo
    :section_string: @todo
    """
    def calc_fft(timeseries):
        """function to create fft
        
        :timeseries: pandas series containing the data you want to create a fft of 
        :returns: t,y,frq,Y

                fft_dict[exp][section]=
        """

        Fs = 1.0;  # sampling rate
        Ts = 1.0/Fs; # sampling interval

        y=timeseries
        # print len(y)
        t = np.arange(0,len(y),Ts) # time vector
        # y = 2*np.sin(2*np.pi*ff*t)
        # y=pd.HDFStore('./nemo_cordex24_ERAI01_timeseries_summarytransports_table.h5').DF['FE'] # time vector

        ff = 5;   # frequency of the signal
        # y = 2*np.sin(2*np.pi*ff*t)

        n = len(y) # length of the signal
        k = np.arange(n)
        T = n/Fs
        frq = k/T # two sides frequency range
        frq = frq[range(n/2)] # one side frequency range

        Y = np.fft.fft(y)/n # fft computing and normalization
        Y = Y[range(n/2)]

        return t,y,frq,Y
    fft_ans=calc_fft(dataframe)
    t   =fft_ans[0]
    y   =fft_ans[1]
    frq =fft_ans[2]
    Y   =fft_ans[3]

    pltaxis.plot([1/f for f in frq],abs(Y)**2,color=co,linewidth=1.5,alpha=0.8) # plotting the spectrum
    pltaxis.set_xlim([0,2000])

    return

def plot_raw_timeseries(oisst,imosvel,imoseke,pfol):
    """@todo: Docstring for plot_one
    
    :arg1: @todo
    :returns: @todo
    """

    def plotme(pltaxis,df):
        tserieslen=len(df.index)
        pltaxis.plot(df.index,df,color='k',lw=1)

        #plot mean
        pltaxis.plot(df.index,[df.mean()]*tserieslen,color='r',alpha=0.9,lw=2)

        standard=pd.rolling_std(df,400,center=True)
        dfmean=pd.rolling_mean(df,400,center=True)
        y1=dfmean+standard
        y2=dfmean-standard
        #y1=\
        #[df.mean()+df.std()]*tserieslen

        #y2=\
        #[df.mean()-df.std()]*tserieslen
        pltaxis.fill_between(df.index,y1,y2,color='k',alpha=.4)
        pd.rolling_mean(df,400,center=True).plot(ax=pltaxis)


        #plot a linear regression
        reg=pd.ols(y=pd.Series(df.values),x=pd.Series(np.arange(len(df))))
        pltaxis.plot(df.index,reg.predict().values,color='g',alpha=0.9,lw=2)
        sm.pl_inset_title_box(pltaxis,'R^2 is '+str(np.round(reg.r2,2)),bwidth="14%",location=2)

        return

    dataframe=pd.HDFStore(oisst)
    dfo=dataframe.select('df')
    #import ipdb; ipdb.set_trace()
    #dfo.plot()
    #plt.show()

    dataframe=pd.HDFStore(imosvel)
    dfi=dataframe.select('df')

    dataframe=pd.HDFStore(imoseke)
    dfeke=dataframe.select('df')

    dataframes=[dfo,dfi,dfeke]
    keys=[\
            ['EACxOne','EACxTwo','TFone'],\
            ['FE','JK','AB'],\
            ['EACxOne','EACxTwo','TFone']\
        ]

    onames=[\
            'oisst_timeseries_raw.png',\
            'imos_vvel_timeseries_raw.png',\
            'imoseke_timeseries_raw.png'\
            ]

    for df,dkey,oname in zip(dataframes,keys,onames):
        plt.close('all')
        row=3
        col=1
        fig, axis = plt.subplots(\
            nrows=row, ncols=col, sharex=False, sharey=False,\
            gridspec_kw={'hspace':.225,'wspace':.065}\
                  )
        

        ax=axis[0]
        plotme(ax,df[dkey[0]])
        plt.setp(ax.get_xticklabels(),visible=False)
        sm.pl_inset_title_box(ax,dkey[0],bwidth="20%",location=1)

        ax=axis[1]
        plotme(ax,df[dkey[1]])
        plt.setp(ax.get_xticklabels(),visible=False)
        sm.pl_inset_title_box(ax,dkey[1],bwidth="20%",location=1)

        ax=axis[2]
        plotme(ax,df[dkey[2]])
        sm.pl_inset_title_box(ax,dkey[2],bwidth="20%",location=1)

        path_to_png=pfol+oname
        fig.savefig(path_to_png,dpi=300,bbox_inches='tight')
        it.AddTrkr(path_to_png,{'Created with':os.path.realpath(__file__),'time':datetime.datetime.now().strftime("%Y-%m-%d %H:%M"),'machine':socket.gethostname(),'function_name':inspect.currentframe().f_code.co_name})
        print path_to_png

    #not looking at this section
    del dfi['LM']

    onames=[\
            'oisst_summstats.png',\
            'imos_vvel_summstats.png',\
            'imoseke_summstats.png'\
            ]

    for df,dkey,oname in zip(dataframes,keys,onames):
        import seaborn as sns
        plt.close('all')
        row=3
        col=1
        fig, axis = plt.subplots(\
            nrows=row, ncols=col, sharex=False, sharey=False,\
            gridspec_kw={'hspace':.225,'wspace':.065}\
                  )
        
        ax=axis[0]

        plot_fft(ax,df[dkey[0]],'g')
        plot_fft(ax,df[dkey[1]],'r')
        plot_fft(ax,df[dkey[2]],'b')
        #plt.show()
        ax=axis[1]

        g=sns.violinplot(data=df,ax=ax,legend=False)
        #g.legend_.remove()
        #plt.setp(g.artists, alpha=1, linewidth=1.5, fill=False, edgecolor="k")

        ax=axis[2]
        df['month']=df.index.month
        #prep input for seaborn
        new={}
        for sec in df.columns:
            if sec=='month':
                continue
            new[sec]=df[[sec,'month']]
            new[sec]['Section']=sec
            del new[sec]['month']
            new[sec].columns=['velocity','Section']
        newdf=pd.concat(new.values())
        newdf['month']=newdf.index.month
        g=sns.boxplot(x='Section',y='velocity',data=newdf,ax=ax,hue="month")
        g.legend_.remove()
        plt.setp(g.artists, alpha=1, linewidth=1.5, fill=False, edgecolor="k")

        path_to_png=pfol+oname
        fig.savefig(path_to_png,dpi=300,bbox_inches='tight')
        it.AddTrkr(path_to_png,{'Created with':os.path.realpath(__file__),'time':datetime.datetime.now().strftime("%Y-%m-%d %H:%M"),'machine':socket.gethostname(),'function_name':inspect.currentframe().f_code.co_name})
        print path_to_png

    return


def plot_dayofyear_timeseries(oisst,imosvel,imoseke,pfol):
    """function to plot EKE as day of year for our various EAC / SECC boxes.
    
    :arg1: @todo
    :returns: @todo
    """
    def calc_dayoyear(dataf):
        """
        function to calculate the mean for each day of the year in the timeseries
        """
        ddata=collections.OrderedDict()
        for day in dataf['day'].drop_duplicates().tolist():
            ddata[day]=dataf[dataf['day']==day].mean()
        # print len(ddata)
        return pd.DataFrame(ddata).T.sort_index()
    

    dataframe=pd.HDFStore(oisst)
    dfo=dataframe.select('df')

    dataframe=pd.HDFStore(imosvel)
    dfi=dataframe.select('df')

    dataframe=pd.HDFStore(imoseke)
    dfeke=dataframe.select('df')

    dfo['day']=dfo.index.dayofyear
    dfi['day']=dfi.index.dayofyear
    dfeke['day']=dfeke.index.dayofyear

    dfo=calc_dayoyear(dfo)
    dfi=calc_dayoyear(dfi)
    dfeke=calc_dayoyear(dfeke)

    plt.close('all')
    fig=plt.figure(figsize=(14.0,12.0))
    # fig=plt.figure()
    ax=fig.add_subplot(1, 1,1)
    

    labtrue=True
    for day,row in dfeke.iterrows():
        if labtrue:
            ax.scatter(day,row['SECC'],color='r',label='SECC',alpha=0.2)
            ax.scatter(day,row['EACUPOne'],color='g',label='EACUPOne',alpha=0.2)
            ax.scatter(day,row['EACmain'],color='k',label='EACmain',alpha=0.2)
            # ax.scatter(day,row['NZone'],color='m',label='NZone')
            labtrue=False
        else:
            ax.scatter(day,row['SECC'],color='r',alpha=0.2)
            ax.scatter(day,row['EACUPOne'],color='g',alpha=0.2)
            ax.scatter(day,row['EACmain'],color='k',alpha=0.2)
            # ax.scatter(day,row['NZone'],color='m')

    ax.plot(dfeke.index,pd.rolling_mean(dfeke['SECC'],10,center=True),color='r',lw=2)
    ax.plot(dfeke.index,pd.rolling_mean(dfeke['EACUPOne'],10,center=True),color='g',lw=2)
    ax.plot(dfeke.index,pd.rolling_mean(dfeke['EACmain'],10,center=True),color='k',lw=2)
    ax.set_xlabel('Day of year')
    ax.set_ylabel('EKE')

    plt.legend()
    ax2 = ax.twinx()
    ax2.plot(dfi.index,pd.rolling_mean(dfi['FE'],10,center=True)*-1,color='g',linestyle='--',lw=3,alpha=0.8)
    ax2.set_ylabel('velocity at FE (dashed)', color='g')
    ax.set_xlim([1,366])

    path_to_png=pfol+'imoseke_dayofyear_comp.png'
    fig.savefig(path_to_png,dpi=300,bbox_inches='tight')
    it.AddTrkr(path_to_png,{'Created with':os.path.realpath(__file__),'time':datetime.datetime.now().strftime("%Y-%m-%d %H:%M"),'machine':socket.gethostname(),'function_name':inspect.currentframe().f_code.co_name})
    print path_to_png
    # plt.show()
    # import ipdb
    # ipdb.set_trace()





if __name__ == "__main__":
    LogStart('',fout=False)
    ##########
    #  init  #
    ##########
    
    oisst=\
    ind.oisst

    workdir=\
    ind.workdir

    oisst=workdir+'oisst_temp/tasman_sea_oisst_timeseries_table.h5'
    imosvel=workdir+'velocity_timeseries_imos_table.h5'
    imoseke=workdir+'imos_eke/tasman_sea_imoseke_timeseries_table.h5'

    assert(os.path.exists(oisst)),"can't find oisst hdfstore"
    assert(os.path.exists(imosvel)),"can't find imos velocity hdfstore"
    assert(os.path.exists(imosvel)),"can't find imos eke hdfstore"

    plotfol=ind.plotdir
    sm.mkdir(plotfol)

    #################
    #  The Work...  #
    #################
    # plot_raw_timeseries(oisst,imosvel,imoseke,plotfol)

    plot_dayofyear_timeseries(oisst,imosvel,imoseke,plotfol)

    lg.info('')
    localtime = time.asctime( time.localtime(time.time()) )
    lg.info("Local current time : "+ str(localtime))
    lg.info('SCRIPT ended')
