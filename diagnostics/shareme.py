# coding=utf-8
#!/usr/bin/env python 
#   Author: Christopher Bull. 
#   Affiliation: Climate Change Research Centre and ARC Centre of Excellence for Climate System Science.
#                Level 4, Mathews Building
#                University of New South Wales
#                Sydney, NSW, Australia, 2052
#   Contact: z3457920@unsw.edu.au
#   www:     christopherbull.com.au
#   Date created: Mon, 13 Feb 2017 17:15:10
#   Machine created on: chris-VirtualBox2
#

"""
This module is for commonly used variables and functions for the EAC warming project

"""

import glob

#needed for nemo_movie function...
from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
from _smlogger import _LogStart
import collections

#needed for find_dates
import pandas as pd
import re

import os
import sys
import itertools
import matplotlib

_lg=_LogStart().setup()

#needed for function distory_colormap
import numpy as np
from mpl_toolkits.axes_grid1 import AxesGrid

#for inset axes
#hacked from:
#http://matplotlib.org/examples/axes_grid/inset_locator_demo.html
from mpl_toolkits.axes_grid1.inset_locator import inset_axes, zoomed_inset_axes
from mpl_toolkits.axes_grid1.anchored_artists import AnchoredSizeBar
import inputdirs as indi
import xarray as xr
import pandas as pd
import subprocess
import datetime as dt

def get_imos_time_mean():
    """@todo: Docstring for create_time_mean
    :returns: path to IMOS average

    Example
    --------
    >>> timemeanf=sm.get_imos_time_mean()
    """
    timemeanf=indi.workdir+'IMOS_OceanCurrent_HV_timemean.nc'
    if not os.path.exists(timemeanf):
        _lg.info("Creating mean file using nco: "+timemeanf)
        ifiles=sorted(glob.glob(indi.imosfol+ '*.nc'))
        assert(ifiles!=[]),"glob didn't find anything!"
        subprocess.call('ncra '+ ' '.join(ifiles)+ ' ' + timemeanf,shell=True)
    else:
        _lg.info("File already existed, skipping creation: "+timemeanf)

    return timemeanf

def get_imos_eke_time_mean():
    """function to create time-mean eke from mk_imos_eke.py

    Relies on:
        /home/z3457920/hdrive/repos/eacobs/diagnostics/mk_imos_eke.py
    :returns: path to IMOS eke average

    Example
    --------
    >>> meanekef=sm.get_imos_eke_time_mean()
    """
    timemeanekef=indi.workdir+'IMOS_OceanCurrent_HV_eke_timemean.nc'
    if not os.path.exists(timemeanekef):
        _lg.info("Creating time-mean file using nco: "+timemeanekef)
        ifiles=sorted(glob.glob(indi.workdir+ 'imos_eke/IM*.nc'))
        assert(ifiles!=[]),"glob didn't find anything! Have you ran: mk_imos_eke.py?"
        _lg.info("CDO catting...")
        subprocess.call('cdo cat '+ ' '.join(ifiles)+ ' ' + timemeanekef+'.temp',shell=True)
        _lg.info("CDO time-mean...")
        subprocess.call('cdo timmean '+ timemeanekef+'.temp'+ ' ' +timemeanekef,shell=True)
        os.remove(timemeanekef+'.temp')
    else:
        _lg.info("File already existed, skipping creation: "+timemeanekef)

    return timemeanekef

def construct_imos_index(eke=False):
    """function to create a date file index of the imos files
    
    :returns: pandas dataframe

    Example
    --------
    >>> dfidx=sm.construct_imos_index()
    >>> dfekeidx=sm.construct_imos_index(eke=True)
    """
    efile = indi.workdir +\
            'imos_output_timeindex'+\
            '_table'+ '.h5'
        
    if eke:
        efile = indi.workdir + 'imos_eke/'+\
                'imos_output_timeindex'+\
                '_table'+ '.h5'

    if not os.path.exists(efile):
        _lg.info("Creating index file : "+efile)
        ifiles=sorted(glob.glob(indi.imosfol + 'IM*.nc' ))
        assert(ifiles!=[]),"glob didn't find anything!"

        if eke:
            ifiles=sorted(glob.glob(indi.workdir + 'imos_eke/IM*.nc' ))
            assert(ifiles!=[]),"glob didn't find anything! Have you run mk_imos_eke.py?"

        #removing the time mean from the hovmoller list...
        #if timemeanfile in ifiles:
            #ifiles.remove(timemeanfile)

        times=[]
        files=[]
        tidx=[]
        for f in ifiles:
            #print f
            _lg.info('Working file: ' + f)
            ifile=xr.open_dataset(f)

            times.append(ifile['TIME'][:].values)
            files.append([f]*len(ifile['TIME'][:]))
            tidx.append(np.arange(len(ifile['TIME'][:])).tolist())
            
            ifile.close()
        df=pd.DataFrame({'date':np.hstack(times),'file':np.hstack(files),'tidx':np.hstack(tidx)},index=np.hstack(times))

        #a clobber check here
        try:
            os.remove(efile)
            _lg.info("HDFStore already existed, clobbering!")
        except OSError:
            pass 
        
        #Due to Pandas 'TypeError' had to use put rather than append...
        store = pd.HDFStore(efile,complevel=9, complib='blosc')
        store.put('df',df)
        store.close()
        _lg.info("HDFStore created: " + efile)
        return df
    else:
        _lg.info("HDFStore already existed skipping creation: " + efile)

        df=pd.HDFStore(efile).df
        return df

def construct_oisst_index():
    """function to create a date file index of the oisst files
    
    :returns: pandas dataframe

    Example
    --------
    >>> dfoisst=sm.construct_oisst_index()
    """
    efile = indi.workdir +\
            'oisst_output_timeindex'+\
            '_table'+ '.h5'
        
    if not os.path.exists(efile):
        _lg.info("Creating index file : "+efile)
        ifiles=sorted(glob.glob(indi.oisst + 'av*.nc' ))
        assert(ifiles!=[]),"glob didn't find anything!"

        times=[]
        files=[]
        tidx=[]
        for f in ifiles:
            # print f,os.path.basename(f)[-11:-3]
            _lg.info('Working file: ' + f)

            ifile=xr.open_dataset(f)
            fdate=\
            np.datetime64(dt.date(int(os.path.basename(f)[-11:-7]),int(os.path.basename(f)[-7:-5]),int(os.path.basename(f)[-5:-3])))
            ncdate=\
            ifile.variables['time'].values[0]
            assert(pd.to_datetime(fdate)==pd.to_datetime(ncdate)),"date in file name and netCDF file do not match!"
            times.append(pd.to_datetime(ncdate))
            files.append(f)
            tidx.append(0)
            
            ifile.close()
        df=pd.DataFrame({'date':np.hstack(times),'file':np.hstack(files),'tidx':np.hstack(tidx)},index=np.hstack(times))

        #a clobber check here
        try:
            os.remove(efile)
            _lg.info("HDFStore already existed, clobbering!")
        except OSError:
            pass 
        
        #Due to Pandas 'TypeError' had to use put rather than append...
        store = pd.HDFStore(efile,complevel=9, complib='blosc')
        store.put('df',df)
        store.close()
        _lg.info("HDFStore created: " + efile)
        return df
    else:
        _lg.info("HDFStore already existed skipping creation: " + efile)

        df=pd.HDFStore(efile).df
        return df

def p_imoslmask(ncfile,pltaxis):
    """function to plot imos landmask onto passed plt axis
    
    :ncfile: @todo
    :ax: @todo
    :returns: @todo
    """

    ifile=Dataset(ncfile, 'r')
    x,y=np.meshgrid(ifile.variables['LONGITUDE'][:],ifile.variables['LATITUDE'][:])
    #landmask
    lmask = np.ma.masked_where(
        ifile.variables['UCUR'][0,:].mask ==True,
        ifile.variables['UCUR'][0,:] ) 

    lmask.fill_value=5
    pltaxis.contourf(x,y,lmask.filled(),levels=[4.9,5,5.1],colors=('#858588','#858588'),alpha=.9,lw=0.1) #landmask
    ifile.close()
    return

class change_tick_labels_add_dirs(object):
    """
    adds East longitude axis
    adds South to latitude axis

    Parameters
    ----------
    axes: 
    usetex: 
    xyonly: 

    Returns
    -------
    
    Notes
    -------
    -Needs to be run AFTER any xlim/ylim changes...
    -If you're using LaTeX then you MUST turn on usetext otherwise you get this obsecure error: UnicodeEncodeError: 'ascii' codec can't encode character u...
    
    Example
    --------
    >>> 
    >>> 
    """
    def __init__(self, axes,usetex=False,xyonly=None):
        # super(change_tick_labels_add_dirs, self).__init__()
        self.axes=axes
        self.usetex=usetex
        # print usetex
        # self.xyonly=xyonly
        if xyonly is None:
            self.fixx()
            self.fixy()
        elif xyonly=='x':
            self.fixx()
        elif xyonly=='y':
            self.fixy()
        else:
            _lg.error("I don't know what to do here! Options are 'x' and 'y'")
            sys.exit()

    def fixx(self):
        xlab=self.axes.get_xticks().tolist()
        if not self.usetex:
            xlab=[str(int(long))+u'°E' for long in xlab]
        else:
            xlab=[str(int(long))+r"$\displaystyle ^{\circ} $E" for long in xlab]

        self.axes.set_xticklabels(xlab)
        return

    def fixy(self):
        ylabby=self.axes.get_yticks().tolist()
        #ylab=[str(int(abs(long)))+'S' for long in ylabby if long<0]
        ylab=[]
        for lat in ylabby:
            if lat<0:
                if not self.usetex:
                    ylab.append(str(int(abs(lat)))+u'°S')
                else:
                    ylab.append(str(int(abs(lat)))+r"$\displaystyle ^{\circ} $S")
            elif lat>0:
                if not self.usetex:
                    ylab.append(str(int(abs(lat)))+u'°N')
                else:
                    ylab.append(str(int(abs(lat)))+r"$\displaystyle ^{\circ} $N")
            else:
                ylab.append(str(int(lat)))
        self.axes.set_yticklabels(ylab)
        return

def mkdir(p):
    """make directory of path that is passed"""
    try:
       os.makedirs(p)
       _lg.info("output folder: "+p+ " does not exist, we will make one.")
    except OSError as exc: # Python >2.5
       import errno
       if exc.errno == errno.EEXIST and os.path.isdir(p):
          pass
       else: raise

def fill_box(pltaxis,boxname):
    """@todo: Docstring for fill_box
    
    :pltaxis: @todo
    :boxname: @todo
    :returns: @todo
    
    Example
    --------
    >>> sm.fill_box(ax,'EACxOne')
    """
    pts=indi.boxdefs[boxname]
    pltaxis.fill([pts[0],pts[1],pts[1],pts[0]],[pts[2],pts[2],pts[3],pts[3]],'w', alpha=0.2, edgecolor='r',lw=1.5)
    return

def makebox(pltaxis,text,xpos,ypos,rot=0):
    pltaxis.annotate(text,
    xy = (xpos,ypos),
    xycoords = 'axes fraction',
    horizontalalignment = 'center',
    verticalalignment = 'bottom',
    fontsize = 8.5,
    fontweight = 'light',# animated=True,
    color='w',
    bbox = dict(boxstyle="round", fc='black',
    ec="0.5", alpha=0.7),rotation=rot
    )
    return pltaxis

def draw_section(pltaxis,sec,direction):
        """@todo: Docstring for fill_box
        
        :pltaxis: @todo
        :boxname: @todo
        :returns: @todo
    
        Example
        --------
        >>> sm.draw_section(ax,'FE','zonal')
        """
        if direction=='zonal':
            p1=indi.sectiondefs[sec[0:1]]
            p2=indi.sectiondefs[sec[1:]]
    
            pltaxis.hlines(y=p1[1], xmin=p1[0], xmax=p2[0],linewidth=3, color='black', zorder=1)
        elif direction=='meridional':
                pass

def pl_inset_title_box(ax,title,bwidth="20%",location=1):
    """
    Function that puts title of subplot in a box
    
    :ax:    Name of matplotlib axis to add inset title text box too
    :title: 'string to put inside text box'
    :returns: @todo
    """

    axins = inset_axes(ax,
                       width=bwidth, # width = 30% of parent_bbox
                       height=.30, # height : 1 inch
                       loc=location)

    plt.setp(axins.get_xticklabels(), visible=False)
    plt.setp(axins.get_yticklabels(), visible=False)
    axins.set_xticks([])
    axins.set_yticks([])

    axins.text(0.5,0.3,title,
            horizontalalignment='center',
            transform=axins.transAxes,size=10)
    return

def find_nearest(array,value):
    idx = (np.abs(array-value)).argmin()
    #return array[idx],idx
    return idx

if __name__ == "__main__":                                     #are we being run directly?
    pass
