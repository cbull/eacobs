#!/bin/bash
set -x
WEB=http://thredds.aodn.org.au/thredds/fileServer/IMOS/OceanCurrent/GSLA/DM00/yearfiles/

wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2017_C-20170210T222546Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2017_C-20170209T222531Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2017_C-20170208T222433Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2017_C-20170207T222432Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2017_C-20170206T222406Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2017_C-20170205T222539Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2017_C-20170204T222439Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2017_C-20170203T222520Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2017_C-20170202T222400Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2017_C-20170201T222512Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2017_C-20170131T222457Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2017_C-20170130T222444Z.nc.gz	
wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20170129T222646Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20170128T223056Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20170127T222720Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20170126T222816Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20170125T222622Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20170124T222753Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20170123T222859Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20170122T222743Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20170121T222721Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20170120T222645Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20170119T222832Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20170118T222841Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20170117T222735Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20170116T222723Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20170115T222939Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20170114T222805Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20170113T223018Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20170112T222750Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20170111T222803Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20170110T222848Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20170109T222239Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20170108T222651Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20170107T222751Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20170106T222749Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20170105T222953Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20170104T222616Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20170103T222621Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20170102T222616Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20170101T222757Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161231T222541Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161230T222757Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161229T222628Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161228T222719Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161227T222621Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161226T222601Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161225T222549Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161224T222808Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161223T222724Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161222T222716Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161221T222619Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161220T222520Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161219T222620Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161218T222610Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161217T222738Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161216T222542Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161215T222652Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161214T222552Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161213T222635Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161212T222719Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161211T222626Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161210T222642Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161209T222818Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161208T222552Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161207T222633Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161206T222657Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161205T222601Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161204T222643Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161203T222817Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161202T222626Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161201T222633Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161130T222742Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161129T222720Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161128T222640Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161127T222554Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161126T222806Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161125T223028Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161124T222706Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161123T222657Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161122T222649Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161121T222617Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161120T223616Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161120T222909Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161112T222743Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161111T222600Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161110T230338Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161109T222335Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161108T222323Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161107T222029Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161106T222635Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161105T222716Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161104T223005Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161103T222905Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161102T222600Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161101T222724Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161031T222745Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161030T230701Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161030T225717Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161030T224904Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161027T222655Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161026T225321Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161026T224724Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161026T224050Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161026T223411Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161026T222724Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161025T034924Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161025T012539Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161018T222729Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161017T222653Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161016T222647Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161015T222635Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161014T222539Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161013T222533Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161012T222648Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161011T222651Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161010T222417Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161009T222629Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161008T222524Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161007T222727Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161006T222539Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161005T222729Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161004T222840Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161003T222751Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161002T222726Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20161001T222730Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20160930T232744Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2016_C-20160929T034139Z.nc.gz	
wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2015_C-20151201T230518Z.nc.gz	
#wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2015_C-20150521T060552Z.nc.gz	
wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2014_C-20150413T032427Z.nc.gz	
wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2013_C-20141028T045015Z.nc.gz	
wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2012_C-20140106T022022Z.nc.gz	
wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2011_C-20141028T044752Z.nc.gz	
wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2010_C-20140120T231433Z.nc.gz	
wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2009_C-20140120T232133Z.nc.gz	
wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2008_C-20150521T043304Z.nc.gz	
wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2007_C-20150521T042748Z.nc.gz	
wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2006_C-20150521T042256Z.nc.gz	
wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2005_C-20150521T041806Z.nc.gz	
wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2004_C-20150521T041319Z.nc.gz	
wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2003_C-20150521T040831Z.nc.gz	
wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2002_C-20150521T040330Z.nc.gz	
wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2001_C-20150521T035914Z.nc.gz	
wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_2000_C-20150521T035421Z.nc.gz	
wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_1999_C-20150521T034853Z.nc.gz	
wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_1998_C-20150521T034357Z.nc.gz	
wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_1997_C-20150521T033755Z.nc.gz	
wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_1996_C-20150521T033049Z.nc.gz	
wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_1995_C-20150521T032432Z.nc.gz	
wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_1994_C-20150521T031623Z.nc.gz	
wget -t 4 –O ${WEB}IMOS_OceanCurrent_HV_1993_C-20150521T030649Z.nc.gz	

gunzip -d IMOS_OceanCurrent_HV_2017_C-20170210T222546Z.nc.gz	
gunzip -d IMOS_OceanCurrent_HV_2016_C-20170129T222646Z.nc.gz	
gunzip -d IMOS_OceanCurrent_HV_2015_C-20151201T230518Z.nc.gz	
gunzip -d IMOS_OceanCurrent_HV_2014_C-20150413T032427Z.nc.gz	
gunzip -d IMOS_OceanCurrent_HV_2013_C-20141028T045015Z.nc.gz	
gunzip -d IMOS_OceanCurrent_HV_2012_C-20140106T022022Z.nc.gz	
gunzip -d IMOS_OceanCurrent_HV_2011_C-20141028T044752Z.nc.gz	
gunzip -d IMOS_OceanCurrent_HV_2010_C-20140120T231433Z.nc.gz	
gunzip -d IMOS_OceanCurrent_HV_2009_C-20140120T232133Z.nc.gz	
gunzip -d IMOS_OceanCurrent_HV_2008_C-20150521T043304Z.nc.gz	
gunzip -d IMOS_OceanCurrent_HV_2007_C-20150521T042748Z.nc.gz	
gunzip -d IMOS_OceanCurrent_HV_2006_C-20150521T042256Z.nc.gz	
gunzip -d IMOS_OceanCurrent_HV_2005_C-20150521T041806Z.nc.gz	
gunzip -d IMOS_OceanCurrent_HV_2004_C-20150521T041319Z.nc.gz	
gunzip -d IMOS_OceanCurrent_HV_2003_C-20150521T040831Z.nc.gz	
gunzip -d IMOS_OceanCurrent_HV_2002_C-20150521T040330Z.nc.gz	
gunzip -d IMOS_OceanCurrent_HV_2001_C-20150521T035914Z.nc.gz	
gunzip -d IMOS_OceanCurrent_HV_2000_C-20150521T035421Z.nc.gz	
gunzip -d IMOS_OceanCurrent_HV_1999_C-20150521T034853Z.nc.gz	
gunzip -d IMOS_OceanCurrent_HV_1998_C-20150521T034357Z.nc.gz	
gunzip -d IMOS_OceanCurrent_HV_1997_C-20150521T033755Z.nc.gz	
gunzip -d IMOS_OceanCurrent_HV_1996_C-20150521T033049Z.nc.gz	
gunzip -d IMOS_OceanCurrent_HV_1995_C-20150521T032432Z.nc.gz	
gunzip -d IMOS_OceanCurrent_HV_1994_C-20150521T031623Z.nc.gz	
gunzip -d IMOS_OceanCurrent_HV_1993_C-20150521T030649Z.nc.gz	

