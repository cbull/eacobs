#!/bin/bash
set -x

#https://www.ncdc.noaa.gov/oisst/data-access

#Root dir:
#ftp://eclipse.ncdc.noaa.gov/pub/OI-daily-v2/NetCDF/

#hopefully the same SST data that Eric Oliver uses

#Download date: 13.02.2017

#wget -r ftp://eclipse.ncdc.noaa.gov/pub/OI-daily-v2/NetCDF/1993/AVHRR/*.gz
#wget -r ftp://eclipse.ncdc.noaa.gov/pub/OI-daily-v2/NetCDF/1994/AVHRR/*.gz
#wget -r ftp://eclipse.ncdc.noaa.gov/pub/OI-daily-v2/NetCDF/1995/AVHRR/*.gz
#wget -r ftp://eclipse.ncdc.noaa.gov/pub/OI-daily-v2/NetCDF/1996/AVHRR/*.gz

#wget -r ftp://eclipse.ncdc.noaa.gov/pub/OI-daily-v2/NetCDF/1997/AVHRR/*.gz
#wget -r ftp://eclipse.ncdc.noaa.gov/pub/OI-daily-v2/NetCDF/1998/AVHRR/*.gz
#wget -r ftp://eclipse.ncdc.noaa.gov/pub/OI-daily-v2/NetCDF/1999/AVHRR/*.gz
#wget -r ftp://eclipse.ncdc.noaa.gov/pub/OI-daily-v2/NetCDF/2000/AVHRR/*.gz
#wget -r ftp://eclipse.ncdc.noaa.gov/pub/OI-daily-v2/NetCDF/2001/AVHRR/*.gz
#wget -r ftp://eclipse.ncdc.noaa.gov/pub/OI-daily-v2/NetCDF/2002/AVHRR/*.gz
#wget -r ftp://eclipse.ncdc.noaa.gov/pub/OI-daily-v2/NetCDF/2003/AVHRR/*.gz
#wget -r ftp://eclipse.ncdc.noaa.gov/pub/OI-daily-v2/NetCDF/2004/AVHRR/*.gz
#wget -r ftp://eclipse.ncdc.noaa.gov/pub/OI-daily-v2/NetCDF/2005/AVHRR/*.gz
#wget -r ftp://eclipse.ncdc.noaa.gov/pub/OI-daily-v2/NetCDF/2006/AVHRR/*.gz
#wget -r ftp://eclipse.ncdc.noaa.gov/pub/OI-daily-v2/NetCDF/2007/AVHRR/*.gz

wget -r ftp://eclipse.ncdc.noaa.gov/pub/OI-daily-v2/NetCDF/2008/AVHRR/*.gz
wget -r ftp://eclipse.ncdc.noaa.gov/pub/OI-daily-v2/NetCDF/2009/AVHRR/*.gz
wget -r ftp://eclipse.ncdc.noaa.gov/pub/OI-daily-v2/NetCDF/2010/AVHRR/*.gz
wget -r ftp://eclipse.ncdc.noaa.gov/pub/OI-daily-v2/NetCDF/2011/AVHRR/*.gz
wget -r ftp://eclipse.ncdc.noaa.gov/pub/OI-daily-v2/NetCDF/2012/AVHRR/*.gz
wget -r ftp://eclipse.ncdc.noaa.gov/pub/OI-daily-v2/NetCDF/2013/AVHRR/*.gz
wget -r ftp://eclipse.ncdc.noaa.gov/pub/OI-daily-v2/NetCDF/2014/AVHRR/*.gz
wget -r ftp://eclipse.ncdc.noaa.gov/pub/OI-daily-v2/NetCDF/2015/AVHRR/*.gz
wget -r ftp://eclipse.ncdc.noaa.gov/pub/OI-daily-v2/NetCDF/2016/AVHRR/*.gz
wget -r ftp://eclipse.ncdc.noaa.gov/pub/OI-daily-v2/NetCDF/2017/AVHRR/*.gz
